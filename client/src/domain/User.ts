import { v4 } from 'uuid'
import { Pronouns } from '../shared/types'

export class User{
    constructor(options? :{ userId: string, handle?: string, email?: string, pronouns?: Pronouns }){
        this.userId = options?.userId || this.userId
        this.handle = options?.handle || this.userId
        this.email = options?.email || undefined
        this.pronouns = options?.pronouns || Pronouns.They
    }
    userId: string = v4()
    handle: string
    email: string | undefined
    pronouns: Pronouns = Pronouns.They
}
