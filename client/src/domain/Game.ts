import { Pronouns } from '../shared/types'

export type GamePlayer = {
    userId: string
    handle: string
    pronouns: Pronouns
    didExit: boolean
    computerPlayerId?: string
}

export type Game = {
    gameId: string
    status: GameStatus
    chatRoomId: string
    hostUserId: string
    hostHandle: string
    createDate: Date
    gameRuleSetName: string
    minPlayers: number
    maxPlayers: number
    players: Array<GamePlayer>
    // options: 
    // deckSetId: string
    // deckName: string
    // deckImageBaseURL: string
    // deckCards: string
    gameState: GameState
}

export enum GameStatus {
    Ready = 'READY',
    InProgress = 'IN PROGRESS',
    Complete = 'COMPLETE',
    Abandoned = 'ABANDONED'
}

export type GameState = {

    currentRound: {}
    completedRounds: {}
    score: Array<PlayerScore>
}

export type PlayerScore = {
    playerId: string
    score: number
}
