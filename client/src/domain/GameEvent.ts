import { User } from "./User"

export enum GameRuleSets {
    Base = 'Base',
    WinThisGame = 'Win This Game'
}

export enum BaseGameEventTypes {

    Create = 'CREATE',
    Start = 'START',
    Join = 'JOIN',
    Exit = 'EXIT',
    Error = 'ERROR'
}
export enum WinGameEventTypes {
    Win = 'WIN'
}

export type GameEvent = {
    eventId: string
    gameId: string
    gameRuleSetName?: string
    eventType: BaseGameEventTypes | string
    playerUser: User
    eventData: any
}

// export type WinThisGameEvent extends GameEvent {
//     gameRuleSetName: string = GameRuleSets.WinThisGame
//     eventType: BaseGameEventTypes | WinGameEventTypes
// }