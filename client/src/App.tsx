import React, { useContext, useEffect } from 'react'
import { Route, Switch, Redirect, NavLink, useHistory } from 'react-router-dom'
import './styles/App.scss'
import Lobby from './pages/Lobby'
import Profile from './pages/Profile'
import Nothing from './pages/Nothing'
import Win from './pages/Win'
import { defaultChannels, Message } from './services/MessageBus'
import { AppContext } from './AppContext'
export const globalNavWait = 30


function App() {
    console.log('app render')
    const context = useContext(AppContext)
    useEffect(() => {
        context.messageBus.init()
    })
    const history = useHistory()
    const onMessage = (message: Message) => {
        console.log(`app on message: ${message.data}`)
        if(message.data.toString().includes("poke") && message.data.toString().includes("bear")){
            console.log(`app setting bear poked true`)
            setTimeout(() => {
                console.log('App: pushing to history')
                history.push('/satiated-bear')
            }, globalNavWait)
        }
    }
    context.messageBus.subscribe({clientId: 'App',  callback: onMessage, channels: [defaultChannels.global]});

    return (
        <div className="App">
            <div className="top-menu">
                <NavLink activeClassName="active" exact to="/">lobby</NavLink>
                <NavLink activeClassName="active" exact to="/profile">profile</NavLink>
                <NavLink activeClassName="active" to="/nowhere">nowhere</NavLink>
            </div>
            <Switch>
                <Route path="/" exact component={Lobby} />
                <Route path="/profile" exact component={Profile} />
                <Route path="/win/:gameId" exact component={Win} />
                <Route path="/win" exact component={Win} />
                <Route path='/default' render={() => <Redirect to= "/" />} />
                <Route component={Nothing} />
            </Switch>
        </div>
    )
}

export default App;

