import { MessageBus } from './MessageBus'
import { BaseGameEventTypes, GameEvent, GameRuleSets, WinGameEventTypes } from '../domain/GameEvent'
import { Game } from '../domain/Game'
import { Pronouns } from '../shared/types'
import { v4 } from 'uuid'
import { ApiClient } from './ApiClient'

export type GameList = {
    games: Array<Game>
    lastId: string
    pageSize: number
}

const gameServiceUrl = 'https://zhf7efbcl9.execute-api.us-east-1.amazonaws.com/dev/apiv2'
const gameEventPath = 'games/events'
const gamesPath = 'games'

export class GameEventSvc {
    private messageBus: MessageBus
    private apiClient: ApiClient
    constructor(messageBus: MessageBus, apiClient: ApiClient){
        this.messageBus = messageBus
        this.apiClient = apiClient
    }

    async getActiveGames(startId?: string, pageSize?: number): Promise<GameList>{
        return await this.apiClient.get<GameList>(gameServiceUrl, `${gamesPath}/ready`)
    }

    async getGame(gameId: string): Promise<Game> {
        return await this.apiClient.get<Game>(gameServiceUrl, `${gamesPath}/${gameId}`)
    }

    async host(hostUserId: string, handle: string): Promise<Game> {
        const event: GameEvent = {
            eventId: v4(),
            gameId: v4(),
            gameRuleSetName: GameRuleSets.Base,
            eventType: BaseGameEventTypes.Create,
            playerUser: { userId: hostUserId, handle, email: '', pronouns: Pronouns.They },
            eventData: {}
        }
        const apiResponse = await this.apiClient.post<Game>(gameServiceUrl, gameEventPath, event)
        return apiResponse
        
    }

    async join(userId: string, handle: string, gameId: string): Promise<void> {
        const event: GameEvent = {
            eventId: v4(),
            gameId,
            gameRuleSetName: GameRuleSets.Base,
            eventType: BaseGameEventTypes.Join,
            playerUser: { userId, handle, email: '', pronouns: Pronouns.They },
            eventData: {}
        }
        await this.apiClient.post(gameServiceUrl, gameEventPath, event)
    }

    async start(userId: string, handle: string, gameId: string): Promise<void> {
        const event: GameEvent = {
            eventId: v4(),
            gameId,
            gameRuleSetName: GameRuleSets.Base,
            eventType: BaseGameEventTypes.Start,
            playerUser: { userId, handle, email: '', pronouns: Pronouns.They },
            eventData: {}
        }
        await this.apiClient.post(gameServiceUrl, gameEventPath, event)
    }

    async exit(userId: string, handle: string, gameId: string): Promise<void> {
        const event: GameEvent = {
            eventId: v4(),
            gameId,
            gameRuleSetName: GameRuleSets.Base,
            eventType: BaseGameEventTypes.Exit,
            playerUser: { userId, handle, email: '', pronouns: Pronouns.They },
            eventData: {}
        }
        await this.apiClient.post(gameServiceUrl, gameEventPath, event)
    }
    
    async win(userId: string, handle: string, gameId: string): Promise<void> {
        const event: GameEvent = {
            eventId: v4(),
            gameId,
            gameRuleSetName: GameRuleSets.WinThisGame,
            eventType: WinGameEventTypes.Win,
            playerUser: { userId, handle, email: '', pronouns: Pronouns.They },
            eventData: {}
        }
        await this.apiClient.post(gameServiceUrl, gameEventPath, event)
    }
}
// allows sending info via post 
// uses messagebus to receive game updates
// TBD fallback to polling