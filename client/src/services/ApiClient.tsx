import { default as axios, AxiosResponse } from 'axios'

export class ApiClient {
    async post<T>(domain: string, path: string, body: any): Promise<any>{
        try {
            console.log(`posting ${domain}/${path} with ${JSON.stringify(body)}`)
            const response = await axios.post<T, AxiosResponse<T>>(`${domain}/${path}`, body)
            console.log(`post response was ${JSON.stringify(response)}`)
            return response.data
        } catch(ex) {
            console.log(`rejection in post: ${JSON.stringify(ex)}`)
        }
    }

    async get<T>(domain: string, path: string){
        console.log(`getting  ${domain}/${path}`)
        const response = await axios.get( `${domain}/${path}`) as AxiosResponse<T>
        return response.data
    }
}