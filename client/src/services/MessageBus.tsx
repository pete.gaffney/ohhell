import { IMessageEvent, w3cwebsocket, w3cwebsocket as W3CWebSocket } from 'websocket';
import { User } from '../domain/User';

export type MessageBusClient = {
    clientId: string,
    callback?: ((message: Message) => void),
    channels: string[]
}

export type SocketConnectionHistory = {
    retryInterval: ReturnType<typeof setInterval> | null
    retryingSince: Date | null
    lastSuccessfulConnection: Date | undefined
    totalConnectionMinutes: number
    totalConnectionsMade: number
}

export type Message = {
    channel: string,
    data: any
}

export enum defaultChannels {
    global = 'GLOBAL',
    echo = 'ECHO',
    chat = 'CHAT',
    auth = 'AUTH',
    game = 'GAME'
}

export enum actions {
    sendChat = 'sendChat',
    sendAuthTicket = 'sendAuthTicket'
}

const closingStatuses = [w3cwebsocket.CLOSED, w3cwebsocket.CLOSING]

const url = 'wss://k10p2mhuke.execute-api.us-east-1.amazonaws.com/dev';

console.log(`mb socket module initialized`)

export class MessageBus {
    constructor(options: {userId: string, handle: string}){
        this.userId = options.userId
        this.handle = options.handle
        this.socketClient = new W3CWebSocket(url)
        this.history = {
            retryInterval: null,
            retryingSince: null,
            lastSuccessfulConnection: undefined,
            totalConnectionMinutes: 0,
            totalConnectionsMade: 0
        }
    }
    history: SocketConnectionHistory
    userId: string
    handle: string
    socketClient: W3CWebSocket
    clients: Record<string,MessageBusClient> = {}

    private logNewConnection() {
        if(this.history.retryInterval){
            clearInterval(this.history.retryInterval)
            this.history.retryInterval = null
        }
        this.history.retryingSince = null
        this.history.lastSuccessfulConnection = new Date()
        this.history.totalConnectionsMade++
        console.log(`mb socket client opened ${JSON.stringify(this.history)}`)
    }
    private logDroppedConnection() {
        this.history.totalConnectionsMade++
        if(this.history.lastSuccessfulConnection){
            const ms = Date.now() - this.history.lastSuccessfulConnection.valueOf()
            this.history.totalConnectionMinutes += ms / 60000
        }
        console.log(`mb socket client closed: ${JSON.stringify(this.history)}`)
    }

    init(callback?:() => void){
        console.log('mb init')
        this.socketClient.onopen = () => {
            this.logNewConnection()
            this.sendAuthTicket()
            callback && callback()
        }
        this.socketClient.onmessage = (received: IMessageEvent) => {
            console.log(`mb handling message ${received.data} at ${Date.now()}`)
            if(this.socketClient.OPEN){
                const { channel, data } = JSON.parse(received.data.toString())
                const message: Message = {
                    channel,
                    data
                }
                const clientArray = Object.values(this.clients)
                clientArray.forEach(client => {
                    client.callback && client.channels.includes(channel) && client.callback(message)
                })
            } else {
                console.log(`mb status inactive, ignoring message`)
            }
        }
        this.socketClient.onclose = () => {
            this.logDroppedConnection()
            this.history.retryInterval = setInterval(() => {
                console.log('trying to reconnect')
                this.history.retryingSince = this.history.retryingSince || new Date()
                if(this.history.retryingSince.valueOf() < Date.now() - 5000){
                    this.history.retryingSince = new Date()
                    this.socketClient = new W3CWebSocket(url)
                    this.init()
                }
            }, 1000)
        }
    }

    setUserInfo(user: User){
        this.userId = user.userId
        this.handle = user.handle
    }

    sendAuthTicket(){
        const authTicket = {
            userId: this.userId,
            handle: this.handle
        }
        this.sendMessage(actions.sendAuthTicket, authTicket, defaultChannels.auth)
    }

    sendMessage(action:string, message:any, channel:string) {
        if(closingStatuses.includes(this.socketClient.readyState)){
            //TODO this will still lose messages if multiple coming while reconnecting... need a queue here
            console.log(`mb reopening socket...`)
            this.socketClient = new W3CWebSocket(url)
            this.init(() => this.sendMessage(action, message, channel))
            return
        }
        const payload = JSON.stringify({ action, channel: channel, data: message, sent: Date.now() })
        console.log(`mb sending message ${payload}`)
        this.socketClient.send(payload)
    }

    closeConnection() {
        this.socketClient.close(1000) // normal closure
    }

    subscribe(messageBusClient: MessageBusClient) {
        console.log(`mb: ${Date.now()} subscribing ${messageBusClient.clientId} into ${JSON.stringify(this.clients)}`)
        this.clients[messageBusClient.clientId] = messageBusClient
        console.log(`mb: subscription result is ${JSON.stringify(this.clients)}`)
    }
    
    unsubscribe(clientId: string) {
        console.log(`mb ${Date.now()} unsubscribing ${clientId}`)
        delete(this.clients[clientId])
    }
}
