import { actions, defaultChannels, Message, MessageBus } from './MessageBus'
import { ChatData, Pronouns } from '../shared/types'

export const globalChatRooms = {
    front: { chatRoomId: 'chrm#front', channelName: 'chrm#front' },
    echo: { chatRoomId: 'chrm#echo', channelName: 'chrm#echo' }
}

export type ChatMessage = {
    text: string,
    userId: string,
    displayName: string,
    timeReceived: Date
}

export class ChatSvc {
    messageBus: MessageBus
    constructor(messageBus: MessageBus){
        this.messageBus = messageBus
    }
    send(message:string, chatRoomId:string, chatRoomName:string, fromUid: string, fromHandle: string): void{
        const chatData : ChatData = {
            text: message,
            fromMember: {
                uid: fromUid,
                handle: fromHandle,
                pronouns: Pronouns.They
            },
            toChatRoomId: chatRoomId,
            toChatRoomName: chatRoomId,
            timeOrigin: new Date()
        }
        this.messageBus.sendMessage(actions.sendChat, chatData, defaultChannels.chat)
    }

    convertPayloadToChatMessage(message: Message){
        if(message.channel !== defaultChannels.chat){
            return
        }
        const data = message.data as ChatData
        return {
            text: data.text,
            userId: data.fromMember.uid,
            displayName: data.fromMember.handle,
            timeReceived: new Date()
        } as ChatMessage
    }
}