import React, {FC, FormEvent, useContext, useRef, useState } from 'react'
import { AppContext } from '../AppContext'
import { validate } from 'uuid'
import { User } from '../domain/User'

interface IProfileState {
    userId?: string
    handle?: string,
    responseText: string
}

const Profile: FC<{}> = () => {
    console.log('profile render')
    const context = useContext(AppContext)

    const initialProfileState: IProfileState = {
        userId: context.user.userId,
        handle: context.user.handle,
        responseText: ''
    }
    const [profileState, setProfileState] = useState(initialProfileState)

    const UserIdEntryField = useRef<HTMLInputElement>(null)
    const HandleEntryField = useRef<HTMLInputElement>(null)
    const ResponseSpan = useRef<HTMLSpanElement>(null)

    function startResponseFadeout(delay = 100){
        if(!ResponseSpan || !ResponseSpan.current){
            return
        }
        ResponseSpan.current.className = 'responseText'
        setTimeout(() => {
            if(!ResponseSpan || !ResponseSpan.current){
                return
            }
            ResponseSpan.current.className = 'responseTextFadeOut'
        }, delay);
    }

    const handleSubmit = (e:  FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const userId = UserIdEntryField?.current?.value
        const userHandle = HandleEntryField?.current?.value

        if(!userId || !validate(userId || '')){
            startResponseFadeout()
            setProfileState({...profileState, responseText: 'user id must be valid uuid  ... try pasting from another device, or maybe dont mess with it'})
            return
        }
        if(!userHandle){
            startResponseFadeout()
            setProfileState({...profileState, responseText: 'gotta give us a name to work with, friend'})
            return
        }

        const updatedUser = new User({ ...context.user, userId: userId, handle: userHandle })
        context.setUser(updatedUser)
        context.messageBus.setUserInfo(updatedUser)
        startResponseFadeout()
        setProfileState({...profileState, responseText: 'saved'})
    }

    function getUpdater(stateFieldName: string){
        return (e: React.ChangeEvent<HTMLInputElement>) => setProfileState({...profileState, [stateFieldName]: e.target.value })
    }

    return (
        <div className="biz-form">
        <h1>tell me about yourself, {context.user.handle}</h1>
        <form onSubmit={handleSubmit}>
            <ul>
            <li><h6>(this website is as insecure as it looks)</h6></li>
            <li><label htmlFor="userIdTextField">User ID</label>
                <input type="text" id="userIdTextField"  ref={UserIdEntryField} value={profileState.userId} 
                    onChange={getUpdater('userId')}/></li>
            <li><label htmlFor="handleTextField">Handle</label>
                <input type="text" id="handleTextField"  ref={HandleEntryField} value={profileState.handle} 
                    onChange={getUpdater('handle')}/></li>
            <li><input className="form-button" type="submit" value="save" />
                <span ref={ResponseSpan} className="responseText">{profileState.responseText}</span></li>
            <li></li>
            </ul>
        </form>
        </div>
    )
}

export default Profile;