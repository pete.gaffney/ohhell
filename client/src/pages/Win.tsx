import React, {FC, MouseEventHandler, useContext, useState, useEffect, useCallback} from 'react'
import { AppContext } from '../AppContext'
import { useParams } from "react-router-dom"
import { Game, GameStatus, PlayerScore } from '../domain/Game'
import { defaultChannels, Message } from '../services/MessageBus'

type stateType = {
    winnerUserId: string | undefined,
    winnerUserHandle: string | undefined,
    gameStatus: GameStatus
}

const Win: FC<{}> = (props: any) => {
    const context = useContext(AppContext)
    const [state, setState] = useState<stateType>({} as stateType)
    const routerLocationState = props?.location?.state || {}

    const params = useParams<{gameId: string}>()
    const gameId = params.gameId || routerLocationState.gameId
    const gameMessageClientId = `gameclient${gameId}`

    const setStatusFromGame = useCallback((game: Game) => {
        const highScoringPlayerScore = game.gameState.score.reduce((winner: PlayerScore, next: PlayerScore) => {
            return !winner || winner.score > next.score ? winner : next
        }, {} as PlayerScore)
        const highScoringPlayer = game.players.find(player => player.userId === highScoringPlayerScore.playerId)
        if(game.status === GameStatus.Complete){
            console.log(`highscoring player score is ${JSON.stringify(highScoringPlayerScore)}
                with player ${JSON.stringify(highScoringPlayer)}`)
            console.log(`all players were ${JSON.stringify(game.players)}`)
        }
        setState({
            winnerUserHandle: game.status === GameStatus.Complete ? highScoringPlayer?.handle : undefined,
            winnerUserId: game.status === GameStatus.Complete ? highScoringPlayer?.userId: undefined,
            gameStatus: game.status
        })
    },[])

    const onGameMessage = useCallback((message: Message) => {
        const gameMessage = message.channel === defaultChannels.game ? message.data : undefined
        if(!gameMessage){ // TODO this check should happen in subscription
            return
        }
        console.log(`got game message ${JSON.stringify(gameMessage)}`)
        setStatusFromGame(gameMessage)
    },[setStatusFromGame])

    useEffect(() => {
        if(gameId && !state.gameStatus){
            context.gameEventSvc.getGame(gameId)
                .then(game => {
                    setStatusFromGame(game)
                })
        }
    })

    useEffect(() => {
        const client = {clientId: gameMessageClientId,  callback: onGameMessage, channels: [defaultChannels.game]}
        context.messageBus.subscribe(client)
        return () => {
            context.messageBus.unsubscribe(gameMessageClientId)
        }
    },[context, onGameMessage, gameMessageClientId])

    if(!gameId){
        return <div>
            <h1>oh weird</h1>
            <p>not sure what happened, but it's not clear which game you're trying to play</p>
            <p>maybe hit up the lobby, i guess</p>
        </div>
    }

    const handleStartGame: MouseEventHandler<HTMLButtonElement> = async (e:  React.MouseEvent<HTMLElement>) => {
        e.preventDefault()
        await context.gameEventSvc.start(context.user.userId, context.user.handle, gameId)
        // const game = await context.gameEventSvc.getGame(gameId)
        // setStatusFromGame(game)
    }

    const handleWinGame: MouseEventHandler<HTMLButtonElement> = async (e:  React.MouseEvent<HTMLElement>) => {
        e.preventDefault()
        await context.gameEventSvc.win(context.user.userId, context.user.handle, gameId)
        // const game = await context.gameEventSvc.getGame(gameId)
        // setStatusFromGame(game)
    }


    return (<div>
        <h1>WIN GAME {gameId}</h1>
        {state.gameStatus === GameStatus.Ready &&
            <div key="start button"><button className="form-button" type="submit" onClick={handleStartGame}>start the game</button></div>
        }
        {state.gameStatus === GameStatus.InProgress && <div>
            <div key="instructions"><p>eyyy first to click is the winner</p></div>
            <div key="win button"><button className="form-button" type="submit" onClick={handleWinGame}>win</button></div>
            </div>}
        {state.gameStatus === GameStatus.Complete && 
            <div key="congrats"><span role="img" aria-label="PARTY PARTY PARTY">🎉🎉🎉🎉🎉🎉🎉🎉</span><span >winner is {state.winnerUserHandle}</span><span role="img" aria-label="PARTY PARTY PARTY">🎉🎉🎉🎉🎉🎉🎉🎉</span></div>
        }
        <div></div>
    </div>)
}
export default Win
// gameplay component for win-this-game