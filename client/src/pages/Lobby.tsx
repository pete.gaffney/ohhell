import React, {FC, useContext } from 'react'
import { AppContext } from '../AppContext'
import ChatBox from '../widgets/ChatBox'
import GamesList from '../widgets/GamesList'

const Lobby: FC<{}> = () => {
    console.log('lobby render')
    const context = useContext(AppContext)
    return (
        <div>
        <h1>welcome to the lobby, {context.user.handle}</h1>
        <div className="widget-row">
            <ChatBox></ChatBox>
            <GamesList></GamesList>
         </div>
        </div>
    )
}

export default Lobby;