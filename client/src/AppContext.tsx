import { v4 } from 'uuid';
import React from 'react';
import { MessageBus } from './services/MessageBus'
import { ApiClient } from './services/ApiClient'
import { ChatSvc } from './services/ChatSvc'
import { GameEventSvc } from './services/GameEventSvc';
import { User } from './domain/User'
import { Pronouns } from './shared/types';

export type AppContextType = {
    user: User,
    setUser: (user:User) => {},
    messageBus: MessageBus, // TODO messagebus and apiClient probably shouldn't be in the context - move subscription functionality to chat service
    apiClient: ApiClient,
    chatSvc: ChatSvc,
    gameEventSvc: GameEventSvc
}

const localUserIdKey = 'userId'
const localUserHandleKey = 'userHandle'

function setStoredUser(user: User){
    localStorage.setItem(localUserIdKey, user.userId)
    localStorage.setItem(localUserHandleKey, user.handle)
}

function getStoredUser(): User {
    return {
        userId: localStorage.getItem(localUserIdKey) || '',
        handle: localStorage.getItem(localUserHandleKey) || ''
    } as User
}

const storedUser = getStoredUser()
const initialUser: User = {
    userId: (storedUser.userId || v4()) as string,
    handle: '',
    email: '',
    pronouns: Pronouns.They
}
initialUser.handle = (storedUser.handle || initialUser.userId.slice(0,8))

setStoredUser(initialUser)

const messageBus = new MessageBus({ userId: initialUser.userId, handle: initialUser.handle})
const apiClient = new ApiClient()

const chatSvc = new ChatSvc(messageBus)
const gameEventSvc = new GameEventSvc(messageBus, apiClient)

const userState = getStoredUser()
function setUser(user: User) {
    setStoredUser(user)
    userState.userId = user.userId
    userState.handle = user.handle
}

export const AppContext = React.createContext({
    user: userState,
    setUser,
    messageBus,
    apiClient,
    chatSvc,
    gameEventSvc
} as AppContextType)