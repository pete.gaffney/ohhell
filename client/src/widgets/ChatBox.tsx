import React, {FC, useContext, useReducer, useRef, FormEvent, useCallback, useEffect } from 'react'
import { Message, defaultChannels } from '../services/MessageBus'
import { AppContext } from '../AppContext'
import { globalChatRooms, ChatMessage } from '../services/ChatSvc'

const chatBoxClientId = 'ChatBox'

enum chatLogReducerOptions {
    add = 'add'
}

type Action = {
    type: string,
    message: ChatMessage
}

type State = {
    messages: ChatMessage[]
}
const initialState: State = { 
    messages: [
    ]
}


const chatLogReducer = (state: State, action: Action) => {
    console.log(`chatbox reducer executing`)
    switch(action.type){
        case chatLogReducerOptions.add: {
            const lastTenChats = state.messages.slice(-99)
            const nextChat =  action.message
            const nextState = { ...state, messages: [...lastTenChats,nextChat] }
            console.log(`chatbox newState ${JSON.stringify(nextState)}`)
            return nextState
        }
    default: {
        console.log('invalid reducer action in chatLogReducer')
        return state
    } 
    }
}

const ChatBox: FC<{}> = () => {
    console.log(`chatbot entry`)
    const [chatLogState, chatLogDispatch] = useReducer(chatLogReducer, initialState)
    const context = useContext(AppContext)
    const onMessage = useCallback((message: Message) => {
        const chatMessage = context.chatSvc.convertPayloadToChatMessage(message)
        if(!chatMessage){ // TODO this check should happen in subscription
            return
        }
        console.log(`chatbox calling dispatch with ${JSON.stringify(message)}`)
        const action: Action = { type: chatLogReducerOptions.add, message: chatMessage}
        chatLogDispatch(action)
    },[chatLogDispatch, context.chatSvc])

    const lastItemRef = useRef<HTMLLIElement>(null)
    useEffect(() => {
        lastItemRef?.current?.scrollIntoView({ behavior: 'smooth' })
    })
    
    useEffect(() => {
        const client = {clientId: chatBoxClientId,  callback: onMessage, channels: [defaultChannels.chat]}
        context.messageBus.subscribe(client)
        return () => {
            context.messageBus.unsubscribe(chatBoxClientId)
        }
    },[context, onMessage])
    const textEntryField = useRef<HTMLInputElement>(null)

    const handleSubmit = (e:  FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const text = textEntryField?.current?.value
        context.chatSvc.send(text || '',
            globalChatRooms.front.chatRoomId,
            globalChatRooms.front.channelName,
            context.user.userId,
            context.user.handle);
        textEntryField.current && (textEntryField.current.value = '')
    }



    return (
        <div className="chat-box"><h1>chat</h1>
            <ul>
                {chatLogState.messages.map(getChatline)}
                <li ref={lastItemRef} ></li>
            </ul>
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="what's on your mind, friend?" id="customTextField"  ref={textEntryField} />
            </form>
        </div>
    )
}

function getChatline (message: ChatMessage, index: number){
    return (<li key={'chatline-' + index}>
            <span className="chat-entry-from">{message.displayName}</span><span className="chat-entry-time">{message.timeReceived.toLocaleTimeString(undefined, { hour: '2-digit', minute: '2-digit'  })}</span>
            <p className="chat-entry-text">{message.text}</p>
        </li>)
}

export default ChatBox