import React, {FC, useContext, useState, useEffect, MouseEventHandler } from 'react'
import { useHistory } from 'react-router-dom';
import { AppContext } from '../AppContext'
import { GameList } from '../services/GameEventSvc'

// component to host game, cancel, search, or join

const GamesList: FC<{}> = ()=> {

    const context = useContext(AppContext)
    const history = useHistory();

    const [state, setState] = useState<GameList>()
    useEffect( () => {
        if(!state){
            context.gameEventSvc.getActiveGames()
                .then(result => setState(result))
                .catch(reason => console.log(`error on GamesList init: ${reason.message || JSON.stringify(reason)}`))
        }
    }, [state, context.gameEventSvc, setState])

    const handleHostGame: MouseEventHandler<HTMLButtonElement> = async (e:  React.MouseEvent<HTMLElement>) => {
        e.preventDefault()
        const game = await context.gameEventSvc.host(context.user.userId, context.user.handle)
        if(!game){
            console.log(`error: could not create game`)
            return
        }
        history.push(`/win/`, { gameId: game.gameId })
    }
    const handleJoinGame: MouseEventHandler<HTMLButtonElement> = async (e:  React.MouseEvent<HTMLElement>) => {
        e.preventDefault()
        const li = (e.target as HTMLButtonElement).parentNode as HTMLLIElement
        const gameId = li?.getAttribute('data-item') as string | null
        if(!gameId){
            console.error('expected a gameId here')
            return
        }
        await context.gameEventSvc.join(context.user.userId, context.user.handle, gameId)
        console.log('joined')
        history.push(`/win/`, { gameId: gameId })
    }

    return(
    <div>
        <h1>games list</h1>
        <div><button className="form-button" type="submit" value="host a game" onClick={handleHostGame}>host game</button></div>
        <div>
            <ul>
                <li>created host players/max </li>
                {state?.games.map(game => { 
                    return <li key={`game-li-${game.gameId}`} data-item={game.gameId}>
                            <span>{Math.round((Date.now() - (new Date(game.createDate).valueOf()))/(60000))} min ago</span>
                            <span>  {game.hostHandle}</span>
                            <span>  {game.players.length}/{game.maxPlayers}</span>
                            <button className="form-button" type="submit" value="join"
                                onClick={handleJoinGame}>join</button>
                        </li>})}
            </ul>
        </div>
    </div>
    )
}

export default GamesList