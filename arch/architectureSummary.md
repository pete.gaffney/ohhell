# Architecture Survey - Oh Hell
Survey v0.1 - August 2021

Oh Hell is designed to be a fun, online, destination.  The central feature is a multiplayer game with a witchy / occult vibe, that ultimately will be a subscriber supported place for people to socialize remotely.  The site will include typical community applications (message board, chat) along with a variety of other content, to be developed on an ongoing basis.

This document is an architectural guidance for the Oh Hell multiplayer card game's initial launch.  Initial launch includes (in order) a chat function, the game itself including basic AI, authentication and account management.

Fast follow features include choice of deck, different rulesets, moderation and messageboard. Future development could include a variety of different spaces.

## Scope and Functional Overview
This document describes backend services for the Oh Hell community application, as well as describing a single page app (SPA) frontend.

Oh Hell itself is a trick-taking game, similar to Hearts or Bridge.  [rules].  It's addictive, and can be played at many levels from simple to fairly strategic with enough of a randomness and progressive momentum to keep players interested for most of an evening.

This site is going to use a Tarot-focused version of Oh Hell to build an audience and community.  The goal is to have a game that can be easily tweaked to remain novel, as well as provide a platform for a number of different witchy/occult novelties.  Over time, we hope to make this an internet destination for all things Tarot and magic related.

A basic appeal of the site is that the site itself is the product, rather than the audience's attention and eyeballs (see the rest of the ad-centric web).  We are looking to sustain an interested enough audience that people are willing to pay a modest subscription fee in order to ensure continued function and development.

A sampling of potential diversions on the site:
- a forum for users to generate a tarot spread and have their fortunes auto generated, or workshopped by an audience.
- more robust AI players, including a variety of chat personas
- web store for buying tarot / occult supplies and books
- collaborative fiction games
- a bot that nags you if you're clearly having fun and not paying
- an intentionally / amusingly obnoxious bot that suggests you buy things on our amazon affiliate related to your conversation
- in game video chat
- platform for fortune tellers to build an audience and interact with fans


Aside from the entertainment features, users likely have expectations around application quality in these areas:
- privacy - absence of tracking software, anonymizable user accounts, clear boundaries around data retention
- moderation - keep the trolls out, and foster an appropriate site-wide culture
- security - guest users can cause minimal damage, protection from doxxing, ability to recover and safeguard accounts

## User Roles and Activities

1. Login - Users can login to the site via username and password or MFA.  Password reset link available via stored email or mobile phone.
2. Profile Management - logged in users can update basic profile info and picture, and view others profiles.  
   - UPDATE PROFILE - including handle, profile picture, contact info, password recovery methods
   - VIEW PROFILE - display above for a user (logged in users only)
3. Chat - Chat capability is provided in a number of areas, including waiting room, topic rooms, and within a game.
   - SEND CHAT - Users enter text into a chat window, and this is displayed to other users within the same space.  
   - PURGE CHAT - Chats are not saved to durable storage.  System functionality purges chats from cache within 24 hours.
   - HIDE CHAT - Admin users can delete a chat post.
   - KICK USER - Admin users can ban a user for a defined period of time, or permanently.
4. Messageboard (WIP) - Durable message board posts ?
   - NEW THREAD
   - READ THREAD
   - POST TO THREAD
   - DIRECT MESSAGE
   - HIDE POST
   - WARN USER
   - BAN USER
5. ohhell - game application
   - CREATE - users can create a new game where other players can join.  Creation options include choice of deck, choice of Trump suit (including random per round), min / max human players, and number of computer players.  Game can be scheduled for the future.  Games can be either public or invite only.  Games can have a description advertising any other things the host wants to advertise.
   - INVITE PLAYERS - users can send notifications in site, or email / phone notifications about a game they've invited new players to.  This includes email invites for players not yet added as users to the system.
   - JOIN - users can join a public game, or private game to which they've been invited.
   - START - Host can start a game, which moves all logged in players to the game's room.
   - RESIGN - a player can quit an active game.
   - KICK - host can kick out non-responsive players.
   - DEAL - shuffle deck and start the round.
   - BID - enter a bid once cards are received.
   - PLAY CARD - player submits a card to the round when it is their turn.
   - GET STATE - on behalf of player, client can request an updated game state.

## Nonfunctional Overview
Part of what makes websites fun and addictive is having quick user feedback to actions; responsiveness is a priority for the application.  We should not have any discernable lag when posting comments or interacting with game elements.  We should avoid or minimize any interstitial loading delays.

Hosting cost is a serious concern.  There may be a substantial delay between audience building and revenue generation, so if possible we would like the site to remain in free tier wherever possible. 

Ultimately, we would like to attract an audience of tens (hundreds?) of thousands of monthly subscribers and active players.  Traffic will likely cluster during peak gaming hours, and performance must remain consistent and responsive, so scalability and elasticity of services are important.

Reliability is important in that having a consistent experience is key to reducing "negative fun".  The site should work as advertised with a minimum of unpredictable buggy behavior.


### Application Architectural Characteristics
#### Responsiveness
The site should have negligable lag or delay between a user taking an action and that action appearing on screen for other players.  We are limited in the initial phase by what's possible in free tier, but an initial goal should be average under 200 ms, with a long term goal of under 100ms for most actions.

This, and especially the live / multiplayer aspect, lends itself to some kind of live connection channel where changes are broadcast from to all interested parties.  There are a number of different P2P networking protocols available.  For simplicity, we're going to start with Websockets.  This is actually more of a client / server messaging / broadcast approach than true P2P approach, and does rely on some kind of central state management and communication hub. We will seal off this part of the interface to allow for swapping to something more purely P2P later if need be (ie can't hit our latency goals.)

While a pure P2P approach would likely be more performant, the central communication hub has other advantages. We naturally want a central arbiter for certain game-related secrets, such as contents of the deck and players hands.

In the future, if we took a purely P2P approach, a server holding game secrets could be one peer and could veto illegal moves by a player (ie, using a hacked client to play a card not in the player's hand, not following suit etc).  This could be accomplished by the addition of a client mechanism to disregard illegal moves.

Another consideration is in application storage.  For critical game and chat functionality, we take a cache-first approach: most actions push to a global cache, and persistent storage updates occur on an asynchronous,  fire-and-forget basis.  Database reads are used to populate the cache as players log in to the site, update profiles, or look up messageboard threads that haven't been active. The cache is generally the source of truth, and is lazy loaded as needed from nonvolatile, connectionless storage.

We don't anticipate a need for overly structured data models, so we will take a NOSQL approach with DynamoDB.  This allows us to avoid connection-related overhead associated with traditional relational databases.

#### Scalability & Elasticity
We are targeting a large, concurrent, North America focused audience with the same responsiveness that we would see in test conditions.  We would also expect that usage is clustered around common US recreational times, such as evening hours.  We do not want to spend on idle capacity the rest of the time, so auto-scaling is a critical feature.

To this end, we use a serverless / Lambda approach, that allows for automatic scaling of application resources and does not charge for unused capacity.  This builds in strongly elastic scaling, although cold starts can be an issue.  

If we decide the cold start problem becomes too difficult or costly to overcome (ie reserved instances), an alternative would be embedding our same application logic in AWS Elastic Kubernetes Service.

#### security

Validation areas
   type
   length
   

#### privacy

#### reliability
backup to https? telemetry when things go wrong?

## Architectural Components

Post messages to hellbot
    all game commands
    hellbot will want to get list of websocket connections from a cache or dynamo, publish appropriate message, also return to sender
    What's the latency goal for accepting a message?
    What's the latency goal for publishing a message?
    each different command can point to its own lambda using custom routes, and an action attribute
    lambda pulls most info from cache, including list of listeners in gamespace
    after pushing to cache, may send to storage service?
    after pushing to cache, send response to all applicable listeners

1. Service Lambdas
   1. all lambdas should remain convertable to https endpoints
2. Cache
3. Storage
4. Decoupling Queues

## Data Model and Storage

We are taking a NOSQL approach, with a cache as the live record of truth, and a backing DynamoDB as the persistent store.   
### Entities
#### game
Games represent a single gaming session of some gameRuleSet.  Games have primary key gameId, but can also be referenced by GSI's on hostUser and status.
Fields:
1. gameId (partitionKey)
2. status
3. hostUser
4. createDate
5. gameRuleSetId
6. gameRuleSetName
7. players (minPlayers, maxPlayers, player: [clientId, userId, pronouns, handle, isDealer, isActivePlayer, computerPlayerId])
8. options () - scoring rules? play variant?
9. status ( scheduled, active, complete )
10. deck ( deckSetId, deckImageBaseURL, cards[{suit, value, id}], )
11. gameState
   1. currentHands
   2. currentRound - currentTrick
   3.  completedRounds
   4.  score
Indexes:
GSI StatusGamesByCreateDate status, createDate -> gameId, gameRuleSetId, gameRuleSetName, players, options
Games I Won
Games with player
Games with ruleset
#### gameRuleSet ?
#### chat
   1. chatId
   2. actionDateTime
   3. text
   4. from
      1. clientId, userId, handle, pronouns
   5. targetChannelId
   6. targetChannelType
GSI - targetChannelIdActionDateTime

#### chatRoom
   1. channelId
   2. channelName
   3. ownerUserId
   4. members
   5. isprivate
   6. LogLength
   7. LogDuration
   8. chatLog (last LogLength chats)
#### user
   1. email
   2. handle
   3. pronouns
#### connection
   1. wsid - web socket uuid
   2. client
      1. handle
      2. clientId - client generated UUID
   3. handle
   4. userId
#### deck
fields:
1. deckSetId,
2. deckImageBaseURL
3. cards[{suit, value, id}]


cardgame
announcer
chat
authorizor - returns with own jwt?
messageboard

- listing of each with overall diagram
- scaling and anticipated usage levels
- architectural style for each component
- SLAs
- Tech stack

## Test Strategy



- Unit Tests test correctness of code, one layer at a time using mocked service inputs.
- Service Tests test code functionality by executing public code methods against local versions of storage systems.
- Controller or Handler tests are similar to service tests, but at the topmost level.
- Endpoint tests are run against actual deployed environments

- Unit tests for error conditions in services.
- Unit test business logic in services.
- Unit test lib code because this type of code is so widely used and need to be reliable.
- Service tests for detailed functional specifications and should cover most use cases.
- Controller and handler tests are mostly avoided.  This layer should ONLY translate between transport / network wrapping and the topmost service layer in the domain, and define basic sucess and error output.  There should be no conditional logic, nor calls to multiple different services in these code areas.  We are building out our services with the understanding they could be called by either a lambda or as part of a "server" image of some kind, so it's important to keep as little here as possible.
- Endpoint tests are used to demonstrate a "happy path" across a whole domain, mirroring the general success condition for you and your users.
- Parameterized Endpoint tests are used to ensure proper security conditions are in place for each endpoint.

## Deployment Plan


POST and PUT documents using REST
GET most recent documents on a topic within timeframe
    /games/ohhell/UUID
announcer sends documents to sockets / user subscribed to topics, simplest is topic updated notification, triggering GET

architectural capabilities
