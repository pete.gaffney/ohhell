module.exports = {
    "roots": [
      "./", "./src", "./test"
    ],
    "moduleDirectories": ["node_modules", "src", "src/lib", "test"],
    "testMatch": [
      "**/__tests__/**/*.+(ts|tsx|js)",
      "**/?(*.)+(spec|test).+(ts|tsx|js)"
    ],
    // swc is significantly faster for day to day use
    "transform": {
      "^.+\\.(ts|tsx)$": '@swc/jest',
    },
    // ts-jest uses tsc, and catches certain module errors swc will not
    // "transform": {
    //   "^.+\\.(ts|tsx)$": 'ts-jest',
    // },
    "setupFiles": ["./test/setUnitEnv.js"]
  }
