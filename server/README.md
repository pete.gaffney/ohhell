# Oh Hell server

## Running tests

Unit tests (test.unit.spec.ts) are run via `yarn test unit`.  Unit tests should be created for any module with conditional logic, business logic, or error pathways.  They are stored adjacent to the code they test in src.

Integration tests are stored in the server/test directory, and are presented here in two flavors
- service tests (test.int.spec.ts)
  - run program code against local database or aws resources
- environment tests (test.env.spec.ts)
  - run web requests against a deployed (dev) environment

Service tests can be run in two ways.  The easiest way is to mimic the pipeline's approach, and run `yarn testdocker`.
This resets and uses the source controlled, seeded database at `server/test/testDataVolume/shared-local-instance.db`.  To update this database, delete the file, run vanilla `docker-compose up` (tests will fail, but container will stay up) seed the database as described below, and commit the result.

To run locally (harder), spin up local AWS components using `yarn dynamo` and run the tests as `yarn test int`.

To seed the database, clear the `server/test/testDataVolume/shared-local-instance.db` file, and start up dynamo using `yarn dynamo`.  Then,
```

Environment tests can be run from a workstation via `yarn test env`
