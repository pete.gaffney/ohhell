import testConfig from './testConfig'

export async function waitForDb(delayMs?: number){
    await new Promise(resolve => setTimeout(resolve, delayMs || testConfig.dynamoWritePauseMs))
}

export async function backOffAsyncUntilResolve<T>(thisToUse: any, func: Function, params: any[], maxWaitMs = 18000, waitMs = 0): Promise<T>{
    return backOffAsyncUntilResolveToTarget(() => true, thisToUse, func, params, maxWaitMs, waitMs)
}

export async function backOffAsyncUntilResolveToTarget<T>(targetMatcher: Function, thisToUse: any, func: Function, params: any[], maxWaitMs = 18000, waitMs = 0): Promise<T>{
    let result: T
    try{
        await waitForDb(waitMs)
        result = await func.call(thisToUse, ...params)
        if(!targetMatcher(result)){
            throw new Error('did not match value')
        }
    } catch(ex) {
        if(waitMs < maxWaitMs){
            result = await backOffAsyncUntilResolve<T>(thisToUse, func, params, maxWaitMs, (waitMs || 100) * 2)
        } else throw new Error(`Could not match within ${maxWaitMs}`)
    }
    return result
}

export async function backOffAsyncUntilThrow<T>(thisToUse: any, func: Function, params: any[], maxWaitMs = 18000, waitMs = 0){
    let result: T
    try {
        await waitForDb(waitMs)
        result = await func.call(thisToUse, ...params) as T
        if(waitMs < maxWaitMs){
            result = await backOffAsyncUntilResolve<T>(thisToUse, func, params, maxWaitMs, (waitMs || 100) * 2) as T
        }
    } catch(ex) {
        throw ex
    }
    return result
}
