import { defaultChannels, WebsocketPayload } from '../../../src/lib/apiGatewaySocket/util';
import { v4 } from 'uuid';
import { IMessageEvent, w3cwebsocket as W3CWebSocket } from 'websocket';
import { Pronouns } from '../../../src/lib/storage/storageTypes';
import { globalChatRooms } from '../../../src/chat/ChatRoom';
import { ChatData } from '../../../src/chat/ChatService';
import { AuthTicket } from "../../../src/lib/auth/websocketAuth"
import { ChatRoomMember } from '../../../src/chat/ChatRoomMember';

const url = 'wss://k10p2mhuke.execute-api.us-east-1.amazonaws.com/dev';
const closingStatuses = [W3CWebSocket.CLOSED, W3CWebSocket.CLOSING]

const getAuthTicket = (userId: string, handle: string) => {
    return { 
        action: 'sendAuthTicket', // must match spec in serverless.yml
        channel: defaultChannels.auth, 
        data:     { userId, handle } as AuthTicket,
        sent: new Date()
    } as WebsocketPayload
}

const getSampleChatBody = (userId: string, sendTime: number) => {
    const chatData = {
        text: 'This template is used by integration tests only.',
        fromMember: new ChatRoomMember({
            uid: userId,
            handle: 'user handle',
            pronouns: Pronouns.They,
            lastActivityDate: new Date(sendTime)
        }),
        toChatRoomId: globalChatRooms.front.chatRoomId,
        toChatRoomName: globalChatRooms.front.channelName,
        timeOrigin: new Date(sendTime)
    } as ChatData
    return {
        ...chatData,
        fromMember: {
            ...chatData.fromMember,
            lastActivityDate: sendTime
        },
        timeOrigin: sendTime
    }
}
const getSampleChatMessage = (userId: string, sendTime: number) => ({
    action: 'sendChat',
    channel: 'TEST',
    data:  getSampleChatBody(userId, sendTime)
})


describe('connectionHandler', () => {
    jest.setTimeout(15000)
    let socketClient : W3CWebSocket
    let start : number
    beforeEach(() => {
        start = Date.now()
        socketClient = new W3CWebSocket(url)
    })
    afterEach(() => {
        if(socketClient){
            if(!closingStatuses.includes(socketClient.readyState)){
                socketClient.close()
            }
            // @ts-ignore
            socketClient = null
        }
    })
    it('should accept connection, send auth ticket, handle an initial chat within 1s, close connection', (done) => {
        const userId = v4()
        const handle = `testHandle_${Date.now()}`
        const authTicket = getAuthTicket(userId, handle)
        let inFailState = false
        let isTestComplete = false
        let sendTime: number

        function failureTeardown(ex: Error){
            inFailState = true
            if(socketClient && !closingStatuses.includes(socketClient.readyState)){
                socketClient.close()
            }
            done(ex.message || JSON.stringify(ex))
        }

        socketClient.onopen = function(){
            try {
                expect(Date.now() - start).toBeLessThan(3000)
                socketClient.send(JSON.stringify(authTicket))
                sendTime = Date.now()
                const message = getSampleChatMessage(userId, sendTime)
                setTimeout(() => socketClient.send(JSON.stringify(message)), 2000)
            } catch(ex) { failureTeardown(ex as Error) }
        }
        socketClient.onmessage = function(received: IMessageEvent) {
            try{
                const latency = Date.now() - sendTime
                expect(latency).toBeLessThan(4000)
                const receivedChatBody = JSON.parse(received.data as string).data as ChatData
                const expectedBody = getSampleChatBody(userId, sendTime)
                expect(receivedChatBody.text).toEqual(expectedBody.text)
                expectedBody.timeSent = receivedChatBody.timeSent
                expect(receivedChatBody).toEqual(expectedBody)
                socketClient.close()
            } catch(ex) { failureTeardown(ex as Error) }
        }
        socketClient.onerror = function(error: Error) {
            failureTeardown(error)
        }
        socketClient.onclose = function() {
            isTestComplete = true
            !inFailState && done()
        }
    })
})
