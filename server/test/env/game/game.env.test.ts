import { default as axios, AxiosResponse} from 'axios'
import { Game } from '../../../src/game/Game'
import { v4 } from 'uuid'
import config from '../../../src/serverConfig'
import { BaseGameEventTypes, GameEvent, GameRuleSets } from '../../../src/game/GameEvent'
import { User } from '../../../src/user/User'

jest.setTimeout(30000)

describe('games', () => {
    it('should allow POST GET by id and DELETE', async () => {
        const url = `${config.httpsBaseUrl}/games`

        const newGameRequest: GameEvent = new GameEvent({
            playerUser: new User( { userId: v4()}),
            eventType: BaseGameEventTypes.Create,
            gameRuleSetName: GameRuleSets.Base
        })
        let promise: Promise<AxiosResponse<Game>>
        await expect(promise = axios.post<Game,AxiosResponse<Game>>(`${url}/events`, newGameRequest))
            .resolves.toHaveProperty('status', 201)
        const response = await promise
        const createdGame = response.data as Game
        expect(response.status).toEqual(201)
        expect(createdGame.hostUserId).toEqual(newGameRequest.playerUser.userId)
        expect(createdGame.gameId).toBeTruthy()

        const gameUrl = `${url}/${createdGame.gameId}`
        let getPromise: Promise<AxiosResponse<Game>>
        await expect(getPromise = axios.get(gameUrl)).resolves.toHaveProperty('status', 200 )
        const getGame = (await getPromise).data
        expect(getGame.gameId).toEqual(createdGame.gameId)

        await expect(axios.delete(gameUrl)).resolves.toHaveProperty('status', 200 )

        let get500Promise: Promise<AxiosResponse<Game>>
        await expect(get500Promise = axios.get(gameUrl)).rejects.toThrowError('Request failed with status code 500')
    })
})
