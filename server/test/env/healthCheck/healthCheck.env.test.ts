import { default as axios} from 'axios'
import config from '../../../src/serverConfig'

describe('healthcheck', () => {
    it('should be available for GET', async () => {
        const url = `${config.httpsBaseUrl}/healthcheck`
        let promise: Promise<unknown>
        await expect(promise = axios.get(url)).resolves.toBeTruthy()
        const result = (await promise) as { data: Record<string,string> }
        expect(result.data.health).toEqual('⛥🍷🪄🗡️ Health!')
    })
})