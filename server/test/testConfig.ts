import { ConfigFactory } from '../src/serverConfig'

const factory = new ConfigFactory

const config = {
    dynamoWritePauseMs: factory.getInt('DYNAMO_WRITE_PAUSE_MS', true)
}

factory.checkMissingValues()

export default config