import appConstants from '../src/appConstants'
import config from '../src/serverConfig'
import { DynamoDB } from 'aws-sdk'

export function getLocalDdbClient(): DynamoDB.DocumentClient{
    return new DynamoDB.DocumentClient({
        apiVersion: appConstants.dynamoApiVersion,
        endpoint: config.dynamoEndpoint,
        region: config.awsRegion
    })
}