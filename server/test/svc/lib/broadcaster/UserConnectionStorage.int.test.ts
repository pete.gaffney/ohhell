import 'source-map-support/register'
import { UserConnectionStorage, ConnectionList } from '../../../../src/lib/broadcaster/UserConnectionStorage'
import { v4 } from 'uuid'
import { getLocalDdbClient } from '../../../getLocalDbClient'

function getRandomConnectionList() {
    return {
        userId: v4(),
        connections: [v4(), v4(), v4()]
    }
}

async function cleanArrayOfConnectionLists(connectionLists: ConnectionList[]){
    const storage = new UserConnectionStorage(getLocalDdbClient())
    for(const connectionList of connectionLists){
        await storage.removeConnections(connectionList, connectionList.connections) 
    }
}

const sortFunction = (cl1: ConnectionList, cl2: ConnectionList) => {
    return (cl1.userId > cl2.userId)
        ? 1
        : (cl1.userId < cl2.userId)
            ? -1
            : 0
}

const exampleA = getRandomConnectionList()
const exampleB = getRandomConnectionList()

jest.setTimeout(30000)

describe('UserConnectionStorage', () => {
    afterEach(async ()=>{
        const storage = new UserConnectionStorage(getLocalDdbClient())
        await storage.removeConnections(exampleA, exampleA.connections)
        await storage.removeConnections(exampleB, exampleB.connections)
    })
    it('should write a user connection list', async () => {
        const storage = new UserConnectionStorage(getLocalDdbClient())
        await expect(storage.storeConnectionList(exampleA)).resolves.toBeFalsy()
    }
    )
    it('should read user connection lists', async () => {
        const storage = new UserConnectionStorage(getLocalDdbClient())
        debugger
        await expect(storage.storeConnectionList(exampleA)).resolves.toBeFalsy()
        await expect(storage.storeConnectionList(exampleB)).resolves.toBeFalsy()
        const returned = await storage.getConnectionLists([exampleA.userId, exampleB.userId])
        expect(returned).toHaveLength(2)
        expect(returned.sort(sortFunction)).toEqual([exampleA, exampleB].sort(sortFunction))
    })

    it('should clear a written connection', async () => {
        const storage = new UserConnectionStorage(getLocalDdbClient())
        await expect(storage.storeConnectionList(exampleA)).resolves.toBeFalsy()
        await expect(storage.getConnectionLists([exampleA.userId]))
            .resolves
            .toEqual(expect.arrayContaining([exampleA]))
        const exampleAWithoutSecondConnection: ConnectionList = {
            userId: exampleA.userId,
            connections: [exampleA.connections[0],exampleA.connections[2]]
        }
        await storage.removeConnections(exampleA, [exampleA.connections[1]])
        const returned = await storage.getConnectionLists([exampleA.userId])
        expect(returned).toEqual([exampleAWithoutSecondConnection])
    })

    it('should delete a written connection if last connection removed', async () => {
        const storage = new UserConnectionStorage(getLocalDdbClient())
        await expect(storage.storeConnectionList(exampleA)).resolves.toBeFalsy()
        await expect(storage.getConnectionLists([exampleA.userId]))
            .resolves
            .toEqual(expect.arrayContaining([exampleA]))
        await storage.removeConnections(exampleA, exampleA.connections)
        const returned = await storage.getConnectionLists([exampleA.userId])
        expect(returned).toEqual([])
    })

    it.skip('should write and retrieve 250 connectionlists in three passes', async () => {
        // TODO this test is slow because we have no batch write yet
        const storage = new UserConnectionStorage(getLocalDdbClient())
        const connectionLists = (Array.from({ length: 250 })).map(() => getRandomConnectionList())
        const start = Date.now()
        for(const connectionList of connectionLists){
            await expect(storage.storeConnectionList(connectionList)).resolves.toBeFalsy()
        }
        const storeEnd = Date.now()
        const retrieved = await storage.getConnectionLists(connectionLists.map(item => item.userId))
        const retrieveEnd = Date.now()
        const originalSorted = connectionLists.sort(sortFunction)
        const retrievedSorted = retrieved.sort(sortFunction)
        expect(originalSorted).toEqual(retrievedSorted)
        const sortCompareEnd = Date.now()
        await cleanArrayOfConnectionLists(originalSorted)
        const cleanEnd = Date.now()
    })
})