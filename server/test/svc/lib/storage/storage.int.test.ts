import { DynamoDbStorage } from '../../../../src/lib/storage/storage'
import { Entity } from '../../../../src/lib/storage/storageTypes'
import { v4 } from 'uuid'
import { getLocalDdbClient } from '../../../getLocalDbClient'
import { StorageAdapters } from '../../../../src/storageAdapters'
import { IndexQuerySpec } from '../../../../src/lib/storage/adapters/adapterTypes'
const testExamplePartition = 'testExamplePartition'

describe('storage',() => {
    afterEach(async () => {
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        const results = (await storage.getRawsByPartitionKey({pk: testExamplePartition})) as Array<Entity>
        const deletionKeys = results.map(result => ({ pk: result.pk, sk: result.sk }) )
        await storage.deleteRawsByKeys(deletionKeys)
    })

    it('should put an entity', async () => {
        const exampleKey = {
            pk: testExamplePartition,
            sk: v4(),
        }
        const exampleEntity = {
            ...exampleKey,
            Kind: 'testExample',
            someAttribute: v4()
        }
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        await expect(storage.putEntity(exampleEntity)).resolves.toBeFalsy()
        await expect(storage.getRawByKey(exampleKey)).resolves.toEqual(exampleEntity)
        exampleEntity.someAttribute = v4()
        await expect(storage.putEntity(exampleEntity)).resolves.toBeFalsy()
        await expect(storage.getRawByKey(exampleKey)).resolves.toEqual(exampleEntity)

    })

    it('should create and delete a test entry', async () => {
        const exampleKey = {
            pk: testExamplePartition,
            sk: v4(),
        }
        const exampleEntity = {
            ...exampleKey,
            Kind: 'testExample',
            someAttribute: v4()
        }
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        await expect(storage.putEntity(exampleEntity)).resolves.toBeFalsy()
        await expect(storage.getRawByKey(exampleKey)).resolves.toEqual(exampleEntity)
        await expect(storage.deleteRawsByKeys([exampleKey])).resolves.toBeFalsy()
        await expect(storage.getRawByKey(exampleKey)).rejects.toThrowError('could not find')
    })

    it('should get multiple records', async () => {
        const exampleKeys = [1,2,3].map(() => ({
            pk: testExamplePartition,
            sk: v4(),
        }))
        const exampleEntities = exampleKeys.map(exampleKey => ({
            ...exampleKey,
            Kind: 'testExample',
            someAttribute: v4()
        }))
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        await Promise.all(exampleEntities.map(async exampleEntity => storage.putEntity(exampleEntity)))
        await expect(storage.getRawsByPartitionKey({pk: testExamplePartition})).resolves
            .toEqual(expect.arrayContaining(exampleEntities))
    })

    it('should get multiple records', async () => {
        const exampleKeys = [1,2,3].map(() => ({
            pk: testExamplePartition,
            sk: v4(),
        }))
        const exampleEntities = exampleKeys.map(exampleKey => ({
            ...exampleKey,
            Kind: 'testExample',
            someAttribute: v4()
        }))
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        await Promise.all(exampleEntities.map(async exampleEntity => storage.putEntity(exampleEntity)))
        await expect(storage.getRawsByPartitionKey({pk: testExamplePartition})).resolves
            .toEqual(expect.arrayContaining(exampleEntities))
    })

    it('getRawsByProjectionIndex should query partition for fully projected models on named index', async () => {
        const index: IndexQuerySpec = {
            partitionKeyName: 'gsi1_pk',
            sortKeyName: 'gsi1_sk',
            indexName: 'GSI1',
            indexKeyGenerator: (self: any) => ({ pk: self.status, sk: self.createDate })
        }
        const partition = `testExamplePartition#${v4()}`
        const exampleKeys = [1,2,3,4,5].map(() => ({
            pk: partition,
            sk: v4(),
        }))
        const exampleEntities = exampleKeys.map((exampleKey, i) => ({
            ...exampleKey,
            Kind: 'testIndexExample',
            status: 'Active',
            createDate: (new Date(1999,12,31,23,59,50+i)).valueOf().toString(),
            [index.partitionKeyName]: 'Active',
            [index.sortKeyName]: (new Date(1999,12,31,23,59,50+i)).valueOf().toString(),
            someAttribute: v4()
        }))
        const startKey = index.indexKeyGenerator(exampleEntities[1])
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        await Promise.all(exampleEntities.map(async exampleEntity => storage.putEntity(exampleEntity)))
        const raws = await storage.getRawsByProjectionIndex(startKey, index, 3)
        expect(raws.length).toEqual(3)
    })
    it.todo('getModelsByProjectionIndex')
})
