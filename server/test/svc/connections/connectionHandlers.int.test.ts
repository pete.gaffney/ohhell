import { connectHandler, disconnectHandler, defaultHandler } from '../../../src/connections/connectionHandlers'

describe('module', () => {
    it('should be truthy', () => {
        expect(!!connectHandler && !!disconnectHandler && !!defaultHandler).toBeTruthy
    })
})