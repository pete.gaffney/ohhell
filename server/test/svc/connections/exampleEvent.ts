import { APIGatewayProxyEvent } from "aws-lambda";

export const event: APIGatewayProxyEvent = {
    "headers": {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.5",
        "Cache-Control": "no-cache",
        "Host": "k10p2mhuke.execute-api.us-east-1.amazonaws.com",
        "Origin": "https://ohell.my-lr.net",
        "Pragma": "no-cache",
        "Sec-Fetch-Dest": "websocket",
        "Sec-Fetch-Mode": "websocket",
        "Sec-Fetch-Site": "cross-site",
        "Sec-WebSocket-Extensions": "permessage-deflate",
        "Sec-WebSocket-Key": "RiwWTHQAPqwBYGJosnIjKQ==",
        "Sec-WebSocket-Version": "13",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0",
        "X-Amzn-Trace-Id": "Root=1-6141eb3f-0e1ff6654efd044c023db595",
        "X-Forwarded-For": "66.41.158.119",
        "X-Forwarded-Port": "443",
        "X-Forwarded-Proto": "https"
    },
    "multiValueHeaders": {
        "Accept": [
            "*/*"
        ],
        "Accept-Encoding": [
            "gzip, deflate, br"
        ],
        "Accept-Language": [
            "en-US,en;q=0.5"
        ],
        "Cache-Control": [
            "no-cache"
        ],
        "Host": [
            "k10p2mhuke.execute-api.us-east-1.amazonaws.com"
        ],
        "Origin": [
            "https://ohell.my-lr.net"
        ],
        "Pragma": [
            "no-cache"
        ],
        "Sec-Fetch-Dest": [
            "websocket"
        ],
        "Sec-Fetch-Mode": [
            "websocket"
        ],
        "Sec-Fetch-Site": [
            "cross-site"
        ],
        "Sec-WebSocket-Extensions": [
            "permessage-deflate"
        ],
        "Sec-WebSocket-Key": [
            "RiwWTHQAPqwBYGJosnIjKQ=="
        ],
        "Sec-WebSocket-Version": [
            "13"
        ],
        "User-Agent": [
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0"
        ],
        "X-Amzn-Trace-Id": [
            "Root=1-6141eb3f-0e1ff6654efd044c023db595"
        ],
        "X-Forwarded-For": [
            "66.41.158.119"
        ],
        "X-Forwarded-Port": [
            "443"
        ],
        "X-Forwarded-Proto": [
            "https"
        ]
    },
    "requestContext": {
        "routeKey": "$connect",
        "eventType": "CONNECT",
        "extendedRequestId": "FtGx7HBGIAMF5Sg=",
        "requestTime": "15/Sep/2021:12:46:55 +0000",
        "messageDirection": "IN",
        "stage": "dev",
        "connectedAt": 1631710015333,
        "requestTimeEpoch": 1631710015335,
        // @ts-expect-error
        "identity": {
            "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0",
            "sourceIp": "66.41.158.119"
        },
        "requestId": "FtGx7HBGIAMF5Sg=",
        "domainName": "k10p2mhuke.execute-api.us-east-1.amazonaws.com",
        "connectionId": "FtGx7eZMIAMCLVg=",
        "apiId": "k10p2mhuke"
    },
    "isBase64Encoded": false
}