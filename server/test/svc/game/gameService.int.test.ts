import { GameService, Game, GamePlayer, GameState } from '../../../src/game/GameService'
import { DynamoDbStorage } from '../../../src/lib/storage/storage'
import { Broadcaster } from '../../../src/lib/broadcaster/Broadcaster'
import { getLocalDdbClient } from '../../getLocalDbClient'
import { waitForDb } from '../../testUtil'
import { ChatService } from '../../../src/chat/ChatService'
import { ChatRoom } from '../../../src/chat/ChatRoom'
import { StorageAdapters } from '../../../src/storageAdapters'
import { BaseGameEventTypes, GameEvent, GameRuleSets } from '../../../src/game/GameEvent'
import { User } from '../../../src/user/User'

const broadcaster = {
    postMessageToUsers : jest.fn()
} as unknown as Broadcaster


const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)

const testHostUser = 'test-host-user'
const createGameEvent = new GameEvent({
    eventType: BaseGameEventTypes.Create,
    gameRuleSetName: GameRuleSets.Base,
    playerUser: new User({userId: testHostUser, handle: testHostUser})
})

async function teardownGame(gameService: GameService, game: Game){
    await waitForDb()
    await gameService.deleteGame(game)
}

describe('gameService',() => {
    it('should create and delete a game record', async () => {
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameEvent)
        expect(game).toBeInstanceOf(Game)
        await waitForDb()
        const found = await gameService.getGame(game)
        expect(found).toEqual(game)
        await gameService.deleteGame(game)
        await waitForDb()
        await expect(gameService.getGame(found)).rejects.toThrow()
    })
    it('should create create and retrieve subObjects correctly', async ()=>{
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameEvent)
        await waitForDb()
        const retrieved = await gameService.getGame(game) as Game
        expect(retrieved).toEqual(game)
        expect(retrieved.players).toHaveLength(1)
        expect(retrieved.players[0]).toBeInstanceOf(GamePlayer)
        expect(retrieved.players[0]).toEqual(new GamePlayer({userId: testHostUser, handle: testHostUser}))
        expect(retrieved.gameState).toBeInstanceOf(GameState)
        expect(retrieved.gameState).toEqual(new GameState())
        await teardownGame(gameService, game)
    })
    it('should create and remove a matching chatroom as game CRUDs', async ()=>{
        const gameService = new GameService(storage, broadcaster)
        const chatService = new ChatService(storage, broadcaster)
        const game = await gameService.createGame(createGameEvent)
        await waitForDb()
        const chatRoom = await chatService.getChatRoom(new ChatRoom({chatRoomId: game.gameId, channelName: 'game chat'}))
        expect(chatRoom.members).toEqual([])
        await teardownGame(gameService, game)
    })
    it('should add created game to the list of active games and remove on status change or delete', async ()=>{
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameEvent)
        const game2 = await gameService.createGame(createGameEvent)

        await waitForDb()
        const activeGames = await gameService.getReadyGameList(game, 2)
        expect(activeGames.games.length).toEqual(2)
        expect(activeGames.lastId).toEqual(game2.gameId)
        expect(activeGames.games).toEqual([game,game2])

        const startGameEvent = new GameEvent({
            gameId: game.gameId,
            eventType: BaseGameEventTypes.Start,
            gameRuleSetName: GameRuleSets.Base,
            playerUser: new User({ userId: game.hostUserId })
        })
        await gameService.startGame(startGameEvent)
        await waitForDb()
        const activeGames2 = await gameService.getReadyGameList(game, 2)
        expect(activeGames2.games.length).toEqual(1)
        expect(activeGames2.games).toEqual([game2])
        await teardownGame(gameService, game2)
        await teardownGame(gameService, game)
        const activeGames3 = await gameService.getReadyGameList(game, 2)
        expect(activeGames3.games.length).toEqual(0)
    })
})
