import 'source-map-support/register'
import { DynamoDbStorage } from '../../../src/lib/storage/storage'
import { Broadcaster } from '../../../src/lib/broadcaster/Broadcaster'
import { v4 } from 'uuid'
import { getLocalDdbClient } from '../../getLocalDbClient'
import { ChatService, ChatData } from '../../../src/chat/ChatService'
import { ChatRoom } from '../../../src/chat/ChatRoom'
import { ChatRoomMember } from '../../../src/chat/ChatRoomMember'
import { backOffAsyncUntilResolve, backOffAsyncUntilResolveToTarget, backOffAsyncUntilThrow, waitForDb } from '../../testUtil'
import { StorageAdapters } from '../../../src/storageAdapters'

const broadcaster = {
    postMessageToUsers: jest.fn()
} as unknown as Broadcaster

const chatRoomId = v4()
function getChatRoomOptions(){
    return {
        chatRoomId,
        channelName: 'Test Channel'
    }
}

describe('chatService', () => {

    jest.setTimeout(90000)
    it('should create a chatroom, add a member, and read it back', async () => {
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        const chatService = new ChatService(storage, broadcaster)
        const chatRoom = new ChatRoom(getChatRoomOptions())
        const exampleA = new ChatRoomMember({ uid: v4(), handle: v4(), lastActivityDate: new Date() })
        const exampleB = new ChatRoomMember({ uid: v4(), handle: v4(), lastActivityDate: new Date()  })
        await expect(chatService.addChatRoom(chatRoom)).resolves.toBeFalsy()
        await expect(chatService.addChatMember(chatRoom, exampleA)).resolves.toBeFalsy()
        await expect(chatService.addChatMember(chatRoom, exampleB)).resolves.toBeFalsy()
        const resultChatRoom = await chatService.getChatRoom(chatRoom)
        expect(resultChatRoom.members.length).toEqual(2)
        expect(resultChatRoom.members.map(member => member.uid)).toEqual(expect.arrayContaining([exampleA.uid, exampleB.uid]))
        await chatService.deleteChatRoom(chatRoom)
    })

    it('should add and remove members', async () => {
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        const chatService = new ChatService(storage, broadcaster)
        const chatRoom = new ChatRoom(getChatRoomOptions())
        const exampleA = new ChatRoomMember({ uid: v4(), handle: v4(), lastActivityDate: new Date()  })

        await expect(backOffAsyncUntilResolve(chatService, chatService.addChatRoom, [chatRoom])).resolves.toBeFalsy()
        await expect(backOffAsyncUntilResolve(chatService, chatService.addChatMember,[chatRoom, exampleA])).resolves.toBeFalsy()
        const resultChatRoom = await backOffAsyncUntilResolve<ChatRoom>(chatService, chatService.getChatRoom,[chatRoom])
        expect(resultChatRoom.members.length).toEqual(1)
        expect(resultChatRoom.members.map(member => member.uid)).toEqual(expect.arrayContaining([exampleA.uid]))

        await expect(chatService.removeChatMember(chatRoom, exampleA)).resolves.toBeFalsy()
        const memberLength0 = (chatRoom:ChatRoom) => chatRoom.members.length === 0
        const resultChatRoom2 = await backOffAsyncUntilResolveToTarget<ChatRoom>(memberLength0, chatService, chatService.getChatRoom, [chatRoom])

        expect(resultChatRoom2.members.length).toEqual(0)
        await chatService.deleteChatRoom(chatRoom)
    })

    it('should create a chatroom and delete it', async () => {
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        const chatService = new ChatService(storage, broadcaster)
        const chatRoom = new ChatRoom(getChatRoomOptions())
        await expect(chatService.addChatRoom(chatRoom)).resolves.toBeFalsy()

        const chatRoomResult = await backOffAsyncUntilResolve(chatService, chatService.getChatRoom, [chatRoom])
        expect(chatRoomResult).toEqual(chatRoom)

        await expect(chatService.deleteChatRoom(chatRoom)).resolves.toBeFalsy()

        await expect(backOffAsyncUntilThrow(chatService, chatService.getChatRoom, [chatRoom])).rejects.toThrow(`error getRecord - could not find {"pk":"${chatRoomId}","sk":"${chatRoom.channelName}"}`)
    })

    it('should create a chatRoom, add a member, and send a message', async () => {
        const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)
        const chatService = new ChatService(storage, broadcaster)
        const chatRoom = new ChatRoom(getChatRoomOptions())
        const exampleA = new ChatRoomMember({ uid: v4(), handle: v4(), lastActivityDate: new Date() })
        await expect(chatService.addChatRoom(chatRoom)).resolves.toBeFalsy()
        await expect(chatService.addChatMember(chatRoom, exampleA)).resolves.toBeFalsy()
        const resultChatRoom = await chatService.getChatRoom(chatRoom)
        let sendPromise
        const chatData: ChatData = {
            text: 'This template used for integration test only.',
            fromMember: exampleA,
            toChatRoomId: chatRoom.chatRoomId,
            toChatRoomName: chatRoom.channelName,
            timeOrigin: new Date()
        }
        await expect(sendPromise = chatService.sendChats(chatData)).resolves.toBeFalsy()
        const expectedResponse = {
            channel: 'CHAT',
            data: chatData,
        }
        expect(broadcaster.postMessageToUsers).toHaveBeenCalledWith(expectedResponse, [exampleA.uid])
        await chatService.deleteChatRoom(chatRoom)
    })
})