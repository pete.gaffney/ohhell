import { v4 } from 'uuid'
import { UserService } from '../../../src/user/UserService'
import { User } from '../../../src/user/User'
import { DynamoDbStorage } from '../../../src/lib/storage/storage'
import { getLocalDdbClient } from '../../getLocalDbClient'
import { waitForDb } from '../../testUtil'
import { StorageAdapters } from '../../../src/storageAdapters'


const storage = new DynamoDbStorage(getLocalDdbClient(), StorageAdapters)

describe('userService',() => {
    it('should create, read, and delete a user', async () => {
        const userService = new UserService(storage)
        const user = new User({ userId: v4() })
        await userService.setUser(user)
        await waitForDb()
        const retrieved = await userService.getUser(user)
        expect(retrieved).toBeInstanceOf(User)
        expect(retrieved).toEqual(user)
        await userService.deleteUser(user)
        await waitForDb()
        await expect(userService.getUser(user)).rejects.toThrow()
    })
})