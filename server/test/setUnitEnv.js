// sets default values for all required config items
process.env.TABLE_NAME="ohell_dev"
process.env.API_GATEWAY_PATH="PLACEHOLDER"
process.env.AWS_REGION="us-east-1"
process.env.LOG_SUPRESS_ERROR="true"
process.env.API_SOCKET_URL="wss://k10p2mhuke.execute-api.us-east-1.amazonaws.com/dev"
process.env.API_HTTPS_URL="https://zhf7efbcl9.execute-api.us-east-1.amazonaws.com/dev/apiv2"

process.env.AWS_ACCESS_KEY_ID="something"
process.env.AWS_SECRET_ACCESS_KEY="something"
// docker compose sets this next value differently; override with localhost only
// if it's unset because you're running from workstation bash.
process.env.DYNAMODB_ENDPOINT=process.env.DYNAMODB_ENDPOINT || "http://localhost:8000"
process.env.DYNAMO_WRITE_PAUSE_MS=1000
// process.env.DEBUG=true
process.env.CHAT_INACTIVITY_TIMEOUT_MS=86400000