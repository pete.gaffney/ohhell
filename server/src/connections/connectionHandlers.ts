import { APIGatewayProxyHandler, APIGatewayEvent } from 'aws-lambda'
import { connectionHandlersInit } from './connectionHandlersInit'
import { globalChatRooms} from '../chat/ChatRoom'
import { ChatRoomMember } from '../chat/ChatRoomMember'
import { getConnectionId, getEventBodyData } from '../lib/apiGatewaySocket/util'
import { getUserIdInsecure, AuthTicket } from '../lib/auth/websocketAuth'
import { Logger } from '../lib/logger/Logger'


function getConnectionDetails(event: APIGatewayEvent) {
  const eventBody = getEventBodyData(event)
  const authTicket = eventBody.data as AuthTicket
  const { userId, handle } = getUserIdInsecure(authTicket)
  return {
    userId,
    handle,
    connectionId: getConnectionId(event)
  }
}

export const connectHandler: APIGatewayProxyHandler = async (event) => {
  return { statusCode: 200, body: 'Connected.' };
};

export const sendAuthTicketHandler: APIGatewayProxyHandler = async (event) => {
  try {
    Logger.debug(`sendAuthHandler - processing ${JSON.stringify(event)}`)
    if(!event.requestContext.connectionId){
      throw new Error('no incoming connection id')
    }
    Logger.debug(`sendAuthHandler - has connection id ${event.requestContext.connectionId}`)
    const { chatService, broadcaster } = connectionHandlersInit()
    Logger.debug(`sendAuthHandler - initialized storage`)
    const { userId, handle, connectionId } = getConnectionDetails(event)
    await broadcaster.registerUserConnection(userId, connectionId)
    Logger.debug(`sendAuthHandler - user connection registered`)
    const newMember = new ChatRoomMember({ uid: userId, handle: handle, lastActivityDate: new Date() })
    await chatService.addChatMember(globalChatRooms.front, newMember)
  } catch (err) {
    Logger.error(`error in sendAuthHandler message ${(err as Error).message} - ${JSON.stringify(err)}`)
    return { statusCode: 500, body: 'Failed to authorize: ' + JSON.stringify(err) };
  }
  return { statusCode: 200, body: 'Authorized.' };
};

export const disconnectHandler: APIGatewayProxyHandler = async (event) => {
    try {
      const { userId, connectionId } = getConnectionDetails(event)
      if(!connectionId){
        throw new Error('no incoming connection id')
      }
      const { broadcaster } = connectionHandlersInit()
      await broadcaster.unregisterUserConnection(userId, connectionId)
    } catch (err) {
      return { statusCode: 500, body: 'Failed to disconnect: ' + JSON.stringify(err) };
    }  
    return { statusCode: 200, body: 'Disconnected.' };
};

export const defaultHandler: APIGatewayProxyHandler = async (event) => {
    return { statusCode: 200, body: 'Default handler fired.' };
}
