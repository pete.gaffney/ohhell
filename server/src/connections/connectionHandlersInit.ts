import { DynamoDbStorage } from '../lib/storage/storage'
import { getDynamoDBClient } from '../lib/storage/dynamoDBClientFactory'
import { ChatService } from '../chat/ChatService'
import { Broadcaster } from '../lib/broadcaster/Broadcaster'
import { getApiGatewayManagementApi } from '../lib/broadcaster/apiGatewayClientFactory'
import { UserConnectionStorage } from '../lib/broadcaster/UserConnectionStorage'
import { StorageAdapters } from '../storageAdapters'

export function connectionHandlersInit() {
    const dynamoClient = getDynamoDBClient()
    const storage = new DynamoDbStorage(dynamoClient, StorageAdapters)
    const userConnectionStorage = new UserConnectionStorage(dynamoClient)
    const broadcaster = new Broadcaster(getApiGatewayManagementApi(), userConnectionStorage)
    const chatService = new ChatService(storage, broadcaster)
    return { chatService, broadcaster }
}