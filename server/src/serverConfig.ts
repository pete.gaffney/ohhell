// config object must have fixed elements so autocomplete works
// must be typed as bool / string / number
// must throw exceptions with list of missing variables on app start

import { config as getDotEnvConfig, DotenvConfigOptions } from 'dotenv'

export class ConfigFactory {
    missingValues: Array<string> = []
    getString(variableName: string, required: boolean = false): string {
        const val = process.env[variableName]
        if(required && !val){
            this.missingValues.push(variableName)
        }
        return val || ''
    }
    getBoolean(variableName: string, required: boolean = false): boolean {
        const val = process.env[variableName] || ''
        if(required && !val){
            this.missingValues.push(variableName)
        }
        return !!(val.toLowerCase() === 'true')
    }
    getInt(variableName: string, required: boolean = false): number {
        const val = Number.parseInt(process.env[variableName] || '')
        if(required && (isNaN(val) || val === undefined)){
            this.missingValues.push(variableName)
        }
        return val || 0
    }

    checkMissingValues(){
        if(this.missingValues.length){
            const err = `These env variables must be defined as strings: ${this.missingValues.join(',')}`
            console.error(err)
            throw new Error(err)
        }
    }
}

const factory = new ConfigFactory()

const dotEnvOptions: DotenvConfigOptions = {
    path: `./env/${factory.getString('APP_ENVIRONMENT')}.env`
}
const output = getDotEnvConfig(dotEnvOptions)
if(output.error){
    throw new Error(`Could not load .env using APP_ENVIRONMENT path ${dotEnvOptions.path} - ${output.error.message}`)
}

const config = {
    environment: factory.getString('APP_ENVIRONMENT', true),
    tableName: factory.getString('TABLE_NAME', true),
    apiGatewayPath: factory.getString('API_GATEWAY_PATH', true),
    httpsBaseUrl: factory.getString('API_HTTPS_URL'),
    dynamoEndpoint: factory.getString('DYNAMODB_ENDPOINT', false),
    awsRegion: factory.getString('AWS_REGION', true),
    placeholder: factory.getString('PLACEHOLDER'),
    debug: factory.getBoolean('DEBUG'),
    suppressErrorLog: factory.getBoolean('LOG_SUPRESS_ERROR'),
    apiSocketURL: factory.getString('API_SOCKET_URL'),
    apiPrefix: 'apiv2',
    isLocal: factory.getBoolean('IS_LOCAL'),
    chatInactivityTimeoutMs: factory.getInt('CHAT_INACTIVITY_TIMEOUT_MS')
}

factory.checkMissingValues()

export default config