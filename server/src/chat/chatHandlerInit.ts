import { ChatService } from './ChatService'
import { DynamoDbStorage } from '../lib/storage/storage'
import { getDynamoDBClient } from '../lib/storage/dynamoDBClientFactory'
import { Broadcaster } from '../lib/broadcaster/Broadcaster'
import { UserConnectionStorage } from '../lib/broadcaster/UserConnectionStorage'
import { getApiGatewayManagementApi } from '../lib/broadcaster/apiGatewayClientFactory'
import { StorageAdapters } from '../storageAdapters'

export function chatHandlerInit() {
    const dynamoClient = getDynamoDBClient()
    const storage = new DynamoDbStorage(dynamoClient, StorageAdapters)
    const broadcaster = new Broadcaster(getApiGatewayManagementApi(), new UserConnectionStorage(dynamoClient))
    return { chatService: new ChatService(storage, broadcaster) }
}