import { Pronouns } from '../lib/storage/storageTypes'

export class ChatRoomMember {
    constructor(options? : { uid: string, handle: string, pronouns?: Pronouns, lastActivityDate: Date }){
        this.uid = options?.uid || ''
        this.handle = options?.handle || ''
        this.lastActivityDate = options?.lastActivityDate || new Date(2000,1)
        this.pronouns = options?.pronouns || Pronouns.They
    }
    uid: string
    handle: string
    pronouns: Pronouns
    lastActivityDate: Date
}
