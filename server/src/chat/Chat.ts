import { Pronouns } from '../lib/storage/storageTypes'

export type Chat = {
    chatRoomId: string
    chatId: string
    kind: 'chat',
    actionDateTime: Date,
    text: string,
    fromConnectionId: string,
    fromUserId: string,
    fromHandle: string,
    fromPronouns: Pronouns,
    targetChannelId: string
}