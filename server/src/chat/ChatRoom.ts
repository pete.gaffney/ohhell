import { Chat } from './Chat'
import { ChatRoomMember } from './ChatRoomMember'
import { v4 } from 'uuid'

export class ChatRoom {
    constructor(options? : {
            chatRoomId? : string,
            channelName?: string,
            ownerUserId?: string,
            isPrivate?: boolean,
            LogDurationHours?: number,
            members?: Array<ChatRoomMember>
            }){
        this.chatRoomId = options?.chatRoomId || this.chatRoomId
        this.channelName = options?.channelName || this.channelName
        this.ownerUserId = options?.ownerUserId || this.ownerUserId
        this.isPrivate = (options?.isPrivate === false) ? false : true
        this.LogDurationHours = options?.LogDurationHours || this.LogDurationHours
        this.members = (options?.members?.length) ? options.members : this.members
    }
    chatRoomId: string = v4()
    channelName: string = ''
    ownerUserId: string = ''
    members: Array<ChatRoomMember> = []
    isPrivate: boolean = true
    LogDurationHours: number = 24
    chatLog: Array<Chat> = []
}
export const globalChatRooms = {
    front: new ChatRoom({ chatRoomId: 'chrm#front', channelName: 'chrm#front', isPrivate: false })
}