import { Context, APIGatewayProxyEvent, Callback } from 'aws-lambda'
import { sendChatHandler } from './chatHandler'
import { chatHandlerInit } from './chatHandlerInit'

jest.mock('./chatHandlerInit', () => ({
    chatHandlerInit: jest.fn(),
  }));
const chatService = {
    sendChats: jest.fn()
}

const simpleEvent = {
    body: JSON.stringify({
        data: {
            text: 'this is my data'
        }
    })
} as APIGatewayProxyEvent
const response = { statusCode: 200, body: 'Data sent.' }

describe('sendChatHandler', () => {
    beforeEach(() => {
        ;(chatHandlerInit as jest.Mock).mockClear()
        ;(chatService.sendChats as jest.Mock).mockClear()
    })

    it('calls sendChats with event.body.data', async () => {
        ;(chatHandlerInit as jest.Mock).mockImplementation(() => {
            return { chatService }
        })
        await sendChatHandler(simpleEvent,
            null as unknown as Context,
            null as unknown as Callback)
        expect(chatHandlerInit).toHaveBeenCalledTimes(1)
        expect(chatService.sendChats).toHaveBeenCalledTimes(1)
    })

    it('returns a 200 response', async () => {
        await expect(sendChatHandler(simpleEvent,
            null as unknown as Context,
            null as unknown as Callback)).resolves.toEqual(response)
    })
})
