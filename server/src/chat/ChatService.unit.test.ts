import { DynamoDbStorage} from '../lib/storage/storage'
import { Broadcaster, Payload } from '../lib/broadcaster/Broadcaster'
import { ChatData, ChatService } from './ChatService'
import { globalChatRooms, ChatRoom } from './ChatRoom'
import { ChatRoomMember } from './ChatRoomMember'
import { Pronouns } from '../lib/storage/storageTypes'
import { defaultChannels } from '../lib/apiGatewaySocket/util'


const exampleText = 'example text'
const exampleChatData: ChatData = {
    text: exampleText,
    fromMember: new ChatRoomMember({ uid: 'a', handle: 'a handle', pronouns: Pronouns.She, lastActivityDate: new Date() }),
    toChatRoomId: globalChatRooms.front.chatRoomId,
    toChatRoomName: globalChatRooms.front.channelName,
    timeOrigin: new Date()
}

const exampleChatPayload: Payload = {
    channel: defaultChannels.chat,
    data: exampleChatData
}

const getExampleChatRoom = (date?: Date) => new ChatRoom({
    ...globalChatRooms.front,
    members: [
        new ChatRoomMember({ uid: 'a', handle: 'a handle', pronouns: Pronouns.She, lastActivityDate: date || new Date() }),
        new ChatRoomMember({ uid: 'b', handle: 'b handle', pronouns: Pronouns.He, lastActivityDate: date || new Date() }),
        new ChatRoomMember({ uid: 'c', handle: 'c handle', pronouns: Pronouns.They, lastActivityDate: date || new Date() }),
        new ChatRoomMember({ uid: 'g', handle: 'g handle', pronouns: Pronouns.They, lastActivityDate: date || new Date() }),
    ]
})
const exampleIds = ['b','c','g','a']

const exampleBadResults = {
    failedConnections: ['d','e','f']
}

const storage = {
    getModel: jest.fn(),
    putModel: jest.fn(),
    deleteModel: jest.fn(),
    getModels: jest.fn()

} as unknown as DynamoDbStorage
const broadcaster = {
    postMessageToUsers: jest.fn()
} as unknown as Broadcaster

describe('ChatService',() => {

    beforeEach(() => {
        jest.clearAllMocks()
        ;(broadcaster.postMessageToUsers as jest.Mock).mockImplementation(async () => undefined)
    })

    it('sendChats gets chatRoom and submits userIds to broadcaster', async () => {
        const chatService = new ChatService(storage, broadcaster)
        const chatRoom = getExampleChatRoom()
        chatService.getChatRoom = jest.fn().mockImplementation(async () => chatRoom)
        await chatService.sendChats(exampleChatData)
        expect(broadcaster.postMessageToUsers).toHaveBeenCalledWith(exampleChatPayload, exampleIds)
    })

    it('removeChatMember puts updated members on chatRoom', async () => { 
        const chatRoom = getExampleChatRoom()
        const partialExampleIdDeleteLists = chatRoom.members[1]
        const postDeleteMembers = [chatRoom.members[0], chatRoom.members[2], chatRoom.members[3]]
        const expectedPostDelete = { ...chatRoom, members: postDeleteMembers }
        ;(storage.getModel as jest.Mock).mockImplementation(async () => chatRoom)
        const chatService = new ChatService(storage, broadcaster)

        await expect(chatService.removeChatMember(globalChatRooms.front, partialExampleIdDeleteLists))
            .resolves.toBeFalsy()
        expect(storage.getModel).toHaveBeenCalledWith(globalChatRooms.front)
        expect(storage.putModel).toHaveBeenCalledWith(expectedPostDelete)
    } )
    it('addChatMember adds a member to chatRoom', async () => {
        const chatRoom = getExampleChatRoom()
        const additionalMember = new ChatRoomMember({ uid: 'q', handle: 'q handle', pronouns: Pronouns.They, lastActivityDate: new Date() })
        const expectedChatRoomPut = Object.assign(chatRoom,
            { members: [...chatRoom.members, additionalMember ] })
        ;(storage.getModel as jest.Mock).mockImplementationOnce(async () => chatRoom)
        const chatService = new ChatService(storage, broadcaster)
        await expect(chatService.addChatMember(globalChatRooms.front, additionalMember))
            .resolves.toBeFalsy()
        expect(storage.putModel).toHaveBeenCalledWith(expectedChatRoomPut)
    } )
    it('addChatMember overwrites existing member of chatRoom', async () => {
        const date = new Date()
        const additionalMember = new ChatRoomMember({ uid: 'q', handle: 'q handle', pronouns: Pronouns.They, lastActivityDate: new Date() })
        const duplicateMember = new ChatRoomMember({ uid: 'q', handle: 'q handle 2', pronouns: Pronouns.They, lastActivityDate: new Date() })
        const expectedChatRoomPut = Object.assign(getExampleChatRoom(date),
            { members: [...getExampleChatRoom(date).members, duplicateMember] })
        const roomWithAdditionalMember = getExampleChatRoom(date)
        roomWithAdditionalMember.members.push(additionalMember)
        ;(storage.getModel as jest.Mock).mockImplementationOnce(async () => roomWithAdditionalMember)
        const chatService = new ChatService(storage, broadcaster)
        await expect(chatService.addChatMember(globalChatRooms.front, duplicateMember))
            .resolves.toBeFalsy()
        expect(storage.putModel).toHaveBeenCalledWith(expectedChatRoomPut)
    } )


    it.skip('should join the host to the game chatroom', async ()=>{

    })
    it.skip('should join the connecting player to the chatroom', async ()=>{
        
    })
})
