import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayEvent } from 'aws-lambda'
import { chatHandlerInit } from './chatHandlerInit'
import { Logger } from '../lib/logger/Logger'
import { ChatData } from './ChatService'
import { getEventBodyData } from '../lib/apiGatewaySocket/util'

export const sendChatHandler: APIGatewayProxyHandler = async (event: APIGatewayProxyEvent) => {
    Logger.debug(`sendChatHandler entry with ${JSON.stringify(event.body)}`)
    try {
    const eventBodyData = getEventBodyData(event).data as ChatData
    eventBodyData.timeSent = new Date()
    const { chatService } = chatHandlerInit()
    await chatService.sendChats(eventBodyData)
    } catch (e) {
        Logger.error(`sendChatHandler error: ${JSON.stringify(e)}`)
        return { 
            statusCode: 500,
            body: (e instanceof Error && e.stack) ? e.stack : 'unknown error'
        }
    }
    return { statusCode: 200, body: 'Data sent.' }
}
