import { Storage } from '../lib/storage/storage'
import { Broadcaster } from '../lib/broadcaster/Broadcaster'
import { Logger } from '../lib/logger/Logger'
import { ChatRoom, globalChatRooms } from './ChatRoom'
import { ChatRoomMember } from './ChatRoomMember'
import { defaultChannels } from '../lib/apiGatewaySocket/util'
import config from '../serverConfig'

export type ChatData = {
    text: string
    fromMember: ChatRoomMember
    toChatRoomId: string
    toChatRoomName: string
    timeOrigin: Date
    timeSent?: Date
}

export class ChatService {
    storage: Storage
    broadcaster: Broadcaster
    constructor(storage: Storage, broadcaster: Broadcaster){
        this.storage = storage
        this.broadcaster = broadcaster
    }

    private async updateChatRoomMemberDates(chatData: ChatData, chatRoom: ChatRoom): Promise<ChatRoom> {
        const senderMember = new ChatRoomMember(chatRoom.members.find(member => member.uid === chatData.fromMember.uid)) || chatData.fromMember
        senderMember.lastActivityDate = new Date()
        const cutOffDate = (Date.now() - config.chatInactivityTimeoutMs)
        Logger.debug(`updateChatRoomMemberDates cutoffdate is ${new Date(cutOffDate)}using configured ms ${config.chatInactivityTimeoutMs}`)
        const newMembers = [
            ...chatRoom.members.filter(member => (member.uid !== senderMember.uid) 
                && (member.lastActivityDate.valueOf() > cutOffDate.valueOf() )),
            senderMember
        ]
        // TODO try removing the Chatroom constructor call and passing in a naked object
        // previously this was causing an empty exception to hit the log, which is not helpful
        const updatedChatRoom: ChatRoom = new ChatRoom({...chatRoom, members: newMembers })
        Logger.debug(`ChatService:updateChatRoomMemberDates storing ${JSON.stringify(updatedChatRoom)}`)
        await this.addChatRoom(updatedChatRoom)
        return updatedChatRoom
    }

    async sendChats (chatData: ChatData){
        // TODO race condition
        Logger.debug(`chatService SendChats with chatData ${JSON.stringify(chatData)}`)
        const filterChatRoom = new ChatRoom({ chatRoomId: chatData.toChatRoomId, channelName: chatData.toChatRoomName })
        Logger.debug(`chatService sendChats pulling chatroom with ${JSON.stringify(filterChatRoom)}`)
        const chatRoom = await this.getChatRoom(filterChatRoom)
        Logger.debug(`chatService sendChats got chatRoom ${JSON.stringify(chatRoom)}`)
        const updatedChatRoom = await this.updateChatRoomMemberDates(chatData, chatRoom)
        Logger.debug(`chatService sendChats updated chatRoom ${JSON.stringify(updatedChatRoom)}`)
        const payload = this.getChatPayload(chatData)
        Logger.debug(`chatService sendChats with ${JSON.stringify(updatedChatRoom)}`)
        await this.broadcaster.postMessageToUsers(payload, updatedChatRoom.members.map(member => member.uid))
    }

    async addChatMember(chatRoom: ChatRoom, member: ChatRoomMember){
        Logger.debug(`addChatMember ${JSON.stringify(member)} to ${JSON.stringify(chatRoom)}`)
        const room = await this.storage.getModel<ChatRoom>(chatRoom)
        Logger.debug(`addChatMember fetched ${JSON.stringify(room)}}`)
        const userIdIndex = room.members.findIndex(item => item.uid === member.uid)
        if(userIdIndex === -1){
            room.members.push(member)
        } else {
            room.members[userIdIndex] = member
        }
        Logger.debug(`addChatMember pushing ${JSON.stringify(room)}}`)
        await this.storage.putModel<ChatRoom>(room)
        Logger.debug(`pushed`)
    }

    async removeChatMember(chatRoom: ChatRoom, member: ChatRoomMember){
        const room = await this.storage.getModel<ChatRoom>(chatRoom)
        const toStore = Object.assign(
            new ChatRoom(),
            room,
            { members: room.members.filter(item => item.uid && item.uid !== member.uid)}
        )
        await this.storage.putModel<ChatRoom>(toStore)
    }

    async getChatRoom(chatRoom: ChatRoom): Promise<ChatRoom> {
        return this.storage.getModel<ChatRoom>(chatRoom)
    }

    async addChatRoom(chatRoom: ChatRoom){
        return this.storage.putModel(chatRoom)
    }

    async deleteChatRoom(chatRoom: ChatRoom){
        return this.storage.deleteModels([chatRoom])
    }

    private getChatPayload(chatData: ChatData){
        return { channel: defaultChannels.chat, data:chatData }
    }

    private getConnectionKey(connectionId:string){
        return {
            pk: `cn`,
            sk: `#${connectionId}`
        }
    }

    private getConnectionObject(item: unknown){
        const raw = item as { pk:string, sk:string }
        return {
            connectionId: raw.sk.slice(1)
        }
    }
}