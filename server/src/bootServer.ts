import { FastifyServerOptions, FastifyInstance, fastify } from 'fastify';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { default as helmet } from 'fastify-helmet'
import { default as cors } from 'fastify-cors'

import config from './serverConfig'
import { AppModule } from './App.module';

// This file could probably go in lambda.ts directly, but there are 
// performance and cost advantages to doing as much startup outside the
// handler as possible.


export interface NestApp {
    app: NestFastifyApplication;
    instance: FastifyInstance;
}

export async function bootServer(): Promise<NestApp> {
    console.log(`booting app in APP_ENVIRONMENT ${config.environment}.`)
    const serverOptions: FastifyServerOptions = {logger: true};
    const instance: FastifyInstance = fastify(serverOptions);
    instance.register(helmet, {})
    instance.register(cors, {
        origin: ['http://localhost:3000', 'https://ohell.my-lr.net']
    })
    const app = await NestFactory.create<NestFastifyApplication>(
        AppModule,
        new FastifyAdapter(instance),{logger: ['log', 'error', 'warn', 'debug', 'verbose']}
    );
    app.setGlobalPrefix(config.apiPrefix);
    await app.init();
    return {app, instance};
}
