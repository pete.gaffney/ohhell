import { v4 } from 'uuid'
import { DynamoDbStorage} from '../lib/storage/storage'
import { Broadcaster } from '../lib/broadcaster/Broadcaster'
import { GameService } from './GameService'
import { Game } from './Game'
import { GamePlayer } from './GamePlayer'
import { GameState, GameStatus, PlayerScore } from './GameState'
import { ChatRoom } from '../chat/ChatRoom'
import { BaseGameEventTypes, GameEvent, GameRuleSets, WinGameEventTypes, WinThisGameEvent } from './GameEvent'
import { User } from '../user/User'

const storage = {
    getModel: jest.fn(),
    putModel: jest.fn(),
    deleteModel: jest.fn(),
    getModels: jest.fn()
} as unknown as DynamoDbStorage

const broadcaster = {
    postMessageToUsers: jest.fn()
} as unknown as Broadcaster

const hostUserId = v4()
const hostPlayerUser:User = new User({
    userId: hostUserId,
    handle: 'handle',
    email: 'handle@domain.com'
})
const createGameForHostPlayerEvent = new GameEvent({ 
        playerUser: hostPlayerUser,
        eventType: BaseGameEventTypes.Create,
        gameRuleSetName: GameRuleSets.Base
    })
const dummyWebsocketID = v4()

async function getInitThreePlayers(){
    const gameService = new GameService(storage, broadcaster)
    const game = await gameService.createGame(createGameForHostPlayerEvent)
    const inProgress = Object.assign(new Game(), game, {
        status: GameStatus.InProgress,
        players: [
            new GamePlayer({userId: hostUserId, handle: 'player 1' }),
            new GamePlayer({userId: v4(), handle: 'player 2'}),
            new GamePlayer({userId: v4(), handle: 'player 3'})
        ]
    })
    return {
        gameService,
        game,
        inProgress
    }
}

describe('gameService createGame', () => {
    beforeEach(() => {
        jest.clearAllMocks()
    })
    it('should create a game with gameState and initial host gamePlayer', async ()=>{
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameForHostPlayerEvent)
        expect(storage.putModel).toHaveBeenCalledTimes(2)
        expect(game).toBeInstanceOf(Game)
        expect(game.gameState).toBeInstanceOf(GameState)
        expect(game.players.length).toEqual(1)
        expect(game.players[0]).toBeInstanceOf(GamePlayer)
        expect(game.players[0].userId).toEqual(hostUserId)
    })
    it('should set game status to ready', async ()=>{
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameForHostPlayerEvent)
        expect(game.status).toEqual(GameStatus.Ready)
    })
    it('should create a matching chatroom', async ()=>{
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameForHostPlayerEvent)
        const chatRoom = new ChatRoom({
            chatRoomId: game.gameId,
            channelName: 'game chat',
            ownerUserId: hostUserId,
            members: []
        })
        expect(storage.putModel).toHaveBeenNthCalledWith(2, chatRoom)
    })
})

describe('gameService startGame', () => {
    it('should set game status to in progress', async ()=>{
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameForHostPlayerEvent)
        ;(storage.putModel as jest.Mock).mockReset()
        ;(storage.getModel as jest.Mock).mockImplementation(async () => game)
        const startGameEvent = new GameEvent({
            playerUser: createGameForHostPlayerEvent.playerUser,
            gameId: game.gameId,
            eventType: BaseGameEventTypes.Start,
            gameRuleSetName: GameRuleSets.Base
        })
        await gameService.startGame(startGameEvent)
        expect(storage.putModel).toHaveBeenNthCalledWith(1,{...game, status: GameStatus.InProgress})
    })
    it('should throw 403 if non-host player tries to start', async ()=>{
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameForHostPlayerEvent)
        ;(storage.putModel as jest.Mock).mockReset()
        ;(storage.getModel as jest.Mock).mockImplementation(async () => game)

        const startGameEvent = new GameEvent({
            playerUser: { ...hostPlayerUser, userId: 'malicious actor'},
            gameId: game.gameId,
            eventType: BaseGameEventTypes.Start,
            gameRuleSetName: GameRuleSets.Base
        })
        await expect(gameService.startGame(startGameEvent)).rejects.toThrowError('Game can not be started by nonhost')
        expect(storage.putModel).not.toHaveBeenCalled()
    })
})

describe('gameService joinGame', () => {
    it('should join the connecting player to the game', async ()=>{
        const newPlayerId = v4()
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameForHostPlayerEvent)
        const newPlayers = [...game.players, new GamePlayer({ userId: newPlayerId, handle:'new player' })]
        ;(storage.putModel as jest.Mock).mockReset()
        ;(storage.getModel as jest.Mock).mockImplementation(async () => game)

        const joinGameEvent = new GameEvent({
            playerUser: { ...hostPlayerUser, userId: newPlayerId, handle:'new player' },
            eventType: BaseGameEventTypes.Join,
            gameRuleSetName: GameRuleSets.Base
        })
        await gameService.joinGame(joinGameEvent)
        expect(storage.putModel).toHaveBeenNthCalledWith(1,{...game, players: newPlayers})
    })
    it('should throw an error if the game is already in progress', async ()=>{
        const newPlayerId = v4()
        const gameService = new GameService(storage, broadcaster)
        const game = await gameService.createGame(createGameForHostPlayerEvent)
        game.status = GameStatus.InProgress
        ;(storage.putModel as jest.Mock).mockReset()
        ;(storage.getModel as jest.Mock).mockImplementation(async () => game)

        const joinGameEvent = new GameEvent({
            playerUser: { ...hostPlayerUser, userId: newPlayerId },
            eventType: BaseGameEventTypes.Join,
            gameRuleSetName: GameRuleSets.Base
        })
        await expect(gameService.joinGame(joinGameEvent)).rejects.toThrow('Game can only be joined in Ready status')
    })
})

describe('gameService winGame', () => {
    beforeEach(() => { jest.clearAllMocks() })
    it('should update the score and change status to completed', async () => {
        const { gameService, inProgress } = await getInitThreePlayers()
        ;(storage.putModel as jest.Mock).mockReset()
        ;(storage.getModel as jest.Mock).mockImplementation(async () => inProgress)
        const secondPlayerScores = [
            new PlayerScore({ playerId: inProgress.players[0].userId, score: 0 }),
            new PlayerScore({ playerId: inProgress.players[1].userId, score: 1 }),
            new PlayerScore({ playerId: inProgress.players[2].userId, score: 0 }),
        ]
        const wonGame = Object.assign(new Game(), inProgress, { 
            gameState : { ...inProgress.gameState, score: secondPlayerScores},
            status: GameStatus.Complete
        })
        const winGameEvent = new WinThisGameEvent({
            gameId: inProgress.gameId,
            eventType: WinGameEventTypes.Win,
            gameRuleSetName: GameRuleSets.WinThisGame,
            playerUser: new User({ userId: inProgress.players[1].userId })
        })
        await gameService.winGame(winGameEvent)
        expect(storage.putModel).toHaveBeenCalledWith(wonGame)
    })
    it.skip('should send updated gameState to each player', async () => {

    })
    it.skip('should announce winner to the game chat', async () => {

    })
    it.skip('should announce winner to the global chat', async () => {

    })
})

describe('gameService exitGame', () => {
    beforeEach(() => { jest.clearAllMocks() })
    it('should mark player resigned for non complete game', async () => {
        const { gameService, inProgress } = await getInitThreePlayers()
        ;(storage.putModel as jest.Mock).mockReset()
        ;(storage.getModel as jest.Mock).mockImplementation(async () => inProgress)
        const player2Exits = Object.assign(new Game(), inProgress,
            {
                players: [
                    inProgress.players[0],
                    Object.assign(new GamePlayer(), inProgress.players[1], { didExit: true }),
                    inProgress.players[2]
                ]
            }
        )
        const exitGameEvent = new GameEvent({
            playerUser: new User({ userId: inProgress.players[1].userId }),
            eventType: BaseGameEventTypes.Exit,
            gameRuleSetName: GameRuleSets.Base,
            gameId: inProgress.gameId
        })
        await gameService.exitGame(exitGameEvent)
        expect(storage.putModel).toHaveBeenCalledWith(player2Exits) 
    })
    it('should change the state to completed if last player exits', async () => {
        const { gameService, inProgress } = await getInitThreePlayers()
        inProgress.players = [inProgress.players[0]]
        ;(storage.putModel as jest.Mock).mockReset()
        ;(storage.getModel as jest.Mock).mockImplementation(async () => inProgress)
        const allquit = Object.assign(new Game(), inProgress,
            { 
                players: [new GamePlayer({...inProgress.players[0], didExit: true})],
                status: GameStatus.Abandoned
            }
        )

        const exitGameEvent = new GameEvent({
            playerUser: new User({ userId: inProgress.players[0].userId }),
            eventType: BaseGameEventTypes.Exit,
            gameRuleSetName: GameRuleSets.Base,
            gameId: inProgress.gameId
        })
        await gameService.exitGame(exitGameEvent)
        expect(storage.putModel).toHaveBeenCalledWith(allquit) 
    })
    it.skip('should announce winner to the game chat', async () => {

    })
})

describe('gameService endGame', () => {
    it.skip('should end a game if called by host', async () => {

    })
    it.skip('should change the state to completed', async () => {

    })
    it.skip('should throw a 403 if game ended by nonhost player', async () => {

    })
})


// shuffle
// deal
// placeBid
// playCard
// scoreRound