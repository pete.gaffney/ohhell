import { Pronouns, PronounTexts } from '../lib/storage/storageTypes'

export class GamePlayer {
    constructor(options?: { userId: string, handle: string, pronouns?: Pronouns, didExit? :boolean } ) {
        this.userId = options?.userId || ''
        this.handle = options?.handle || ''
        this.pronouns = options?.pronouns || this.pronouns
        this.didExit = options?.didExit || this.didExit
    }
    userId: string
    handle: string
    pronouns: Pronouns = Pronouns.They
    didExit: boolean = false
    computerPlayerId: string = ''
    get pronounTexts() { return PronounTexts[this.pronouns] }
}