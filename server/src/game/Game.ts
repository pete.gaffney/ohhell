// import {
//     Contains,
//     IsInt,
//     Length,
//     IsEmail,
//     IsFQDN,
//     IsDate,
//     Min,
//     Max,
//   } from 'class-validator'
import { v4 } from 'uuid'
import { GameState, GameStatus } from './GameState'
import { GamePlayer } from './GamePlayer'
import { GameRuleSets } from './GameEvent'

export class Game {
    constructor(options?: { hostUserId?: string, hostHandle?: string, gameId?: string }) {
        this.hostUserId = options?.hostUserId || ''
        this.hostHandle = options?.hostHandle || this.hostUserId
        this.gameId = options?.gameId || v4()
        this.status = GameStatus.Ready
        this.chatRoomId = v4()
        this.createDate = new Date()
        this.gameRuleSetName = GameRuleSets.WinThisGame
        this.minPlayers = 1
        this.maxPlayers = 10
        this.players = []
        this.gameState = new GameState()
    }
    gameId: string
    status: string
    chatRoomId: string
    hostUserId: string
    hostHandle: string
    createDate: Date
    gameRuleSetName: string
    minPlayers: number
    maxPlayers: number
    players: Array<GamePlayer>
    // options: 
    // deckSetId: string
    // deckName: string
    // deckImageBaseURL: string
    // deckCards: string
    gameState: GameState
    get numPlayersNeeded() { return this.maxPlayers - this.minPlayers }
}
