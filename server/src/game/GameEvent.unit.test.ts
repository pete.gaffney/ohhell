import { GameEvent, GameRuleSets, WinGameEventTypes, WinThisGameEvent } from './GameEvent'

describe('GameEvent constructor', () => {
    it('should set event type', () => {
        const body = {
            eventId: "03ee5847-b28b-4831-9274-68ee1a4db57e",
            gameId: "33392073-566b-4adc-9dd2-0f7730b9a0b5",
            gameRuleSetName: "Base",
            eventType: "Create",
            playerUser: {
                userId: "5dc1c300-ef31-4829-b025-25fb91aef178",
                handle: "migf",
                email: "",
                pronouns: "They"
            },
            eventData: {}
        }
        const result = new GameEvent(body as unknown as GameEvent)
        expect(result.playerUser.email).toBeUndefined()
        result.playerUser.email = ''
        expect(result).toEqual(body)
    })

    it('should set event type for WIN', () => {
        const body = {
            eventId: "03ee5847-b28b-4831-9274-68ee1a4db57e",
            gameId: "33392073-566b-4adc-9dd2-0f7730b9a0b5",
            gameRuleSetName: "Win This Game",
            eventType: "WIN",
            playerUser: {
                userId: "5dc1c300-ef31-4829-b025-25fb91aef178",
                handle: "migf",
                email: "someone@here.net",
                pronouns: "They"
            },
            eventData: {}
        }
        const result = new WinThisGameEvent(body as unknown as WinThisGameEvent)
        expect(result.gameRuleSetName).toEqual(GameRuleSets.WinThisGame)
        expect(result.eventType).toEqual(WinGameEventTypes.Win)
        expect(result).toEqual(body)
    })
})