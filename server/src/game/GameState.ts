export enum GameStatus {
    Ready = 'READY',
    InProgress = 'IN PROGRESS',
    Complete = 'COMPLETE',
    Abandoned = 'ABANDONED'
}

export class GameState {
    constructor(){

    }
    currentRound: {} = {}
    completedRounds: {}[] = []
    score: Array<PlayerScore> = []
}

export class PlayerScore {
    constructor(options?:{ playerId: string, score: number }){
        Object.assign(this, options)
    }
    playerId: string = ''
    score: number = 0
}