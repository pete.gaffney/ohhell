import { Body, Controller, Delete, Get, Inject, Param, Post } from '@nestjs/common';
import { Game } from './Game';
import { GameEvent, IGameEvent } from './GameEvent';
import { GameService, GameList } from './GameService';


@Controller('games')
export class GameController {
  private gameService: GameService
  constructor(@Inject(GameService) gameService: GameService) {
    this.gameService = gameService
  }
  @Post()
  async createGame(@Body() body: GameEvent): Promise<Game> {
    return await this.gameService.createGame(new GameEvent(body))
  }
  @Get('/:id')
  async getGameById(@Param('id') id: string): Promise<Game> {
    return await this.gameService.getGame(new Game({gameId: id}))
  }
  @Delete('/:id')
  async deleteGameById(@Param('id') id: string) {
    await this.gameService.deleteGame(new Game({gameId: id}))
  }
  @Post('/events')
  async processGameEvent(@Body() body: IGameEvent): Promise<Game> {
    const result = await this.gameService.processIGameEvent(body)
    return result
  }
  @Get('/ready')
  async getReadyGames(): Promise<GameList> {
    return await this.gameService.getReadyGameList()
  }

}
