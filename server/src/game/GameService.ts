import { Inject, Injectable } from '@nestjs/common';
import { Storage } from '../lib/storage/storage'
import { Pronouns } from '../lib/storage/storageTypes'
import { Broadcaster, Payload } from '../lib/broadcaster/Broadcaster'
import { Game } from './Game'
import { GamePlayer } from './GamePlayer'
import { GameState, GameStatus, PlayerScore } from './GameState'
import { ChatRoom } from '../chat/ChatRoom'
import { gameIndexes } from '../storageAdapters/gameAdapters'
import { GameEvent, GameRuleSets, BaseGameEventTypes, WinGameEventTypes, WinThisGameEvent, IGameEvent } from './GameEvent';
import { defaultChannels } from '../lib/apiGatewaySocket/util';

export { GamePlayer, Game, GameState }

export type GameList = {
    games: Game[],
    lastId: string | undefined,
    pageSize: number
}

@Injectable()
export class GameService {
    private storage: Storage
    private broadcaster: Broadcaster
    private indexes = {
        StatusCreateDate: gameIndexes.StatusCreateDate
    }

    private gameEventConstructors: Record<string, new(...args: any) => any> = {
        [GameRuleSets.Base]: GameEvent,
        [GameRuleSets.WinThisGame]: WinThisGameEvent
    }

    private gameEventHandlers: Record<string, Record<string,Function>> = {
        [GameRuleSets.Base]: {
            [BaseGameEventTypes.Create]: this.createGame,
            [BaseGameEventTypes.Start]: this.startGame,
            [BaseGameEventTypes.Join]: this.joinGame,
            [BaseGameEventTypes.Exit]: this.exitGame,
            [BaseGameEventTypes.Error]: (event: GameEvent) => { throw new Error('Error event is not implemented!') }
        },
        [GameRuleSets.WinThisGame] : {
            [WinGameEventTypes.Win]:  this.winGame
        }
    }

    constructor(@Inject('STORAGE') storage: Storage, @Inject('BROADCASTER')broadcaster: Broadcaster){
        this.storage = storage
        if(!this.storage){
            throw new Error('GameSevice initialization error: this.storage is null')
        }
        this.broadcaster = broadcaster
        if(!this.storage){
            throw new Error('GameSevice initialization error: this.broadcaster is null')
        }
    }
    
    private getGameEvent(event: IGameEvent): IGameEvent{
        const eventConstructor = this.gameEventConstructors[event.gameRuleSetName]
        return new eventConstructor(event)
    }

    private getGameProcessor(event: IGameEvent): Function {
        const gameRuleSetName : string = event.gameRuleSetName || ''
        const gameEventHandler = this.gameEventHandlers[gameRuleSetName]
        if(!gameEventHandler){
            throw new Error(`getGameProcessor - Configuration error.  Game ruleset not supported.
            Supported rulesets are ${JSON.stringify(Object.keys(this.gameEventHandlers))}`)
        }
        if(!gameEventHandler[event.eventType]){
            throw new Error('getGameProcessor - Configuration error.  Game event not supported for ruleset.')
        }
        return gameEventHandler[event.eventType]
    }

    async processIGameEvent(event: IGameEvent){
        const typedEvent = this.getGameEvent(event)
        const processor = this.getGameProcessor(typedEvent)
        return await processor.call(this, typedEvent)
    }

    async createGame(event: GameEvent){
        const hostUserId = event.playerUser.userId
        const handle = event.playerUser.handle
        const game = new Game({hostUserId, hostHandle: handle})
        game.players.push(new GamePlayer({ 
            userId: hostUserId,
            handle,
            pronouns: Pronouns.They
        }))
        game.gameState = new GameState()
        await this.storage.putModel<Game>(game)

        //TODO this should be handled via async message platform
        const chatRoom = this.buildGameChatRoom(game.gameId, hostUserId)
        await this.storage.putModel<ChatRoom>(chatRoom)

        return game
    }
    async startGame(event: GameEvent){
        const gameId = event.gameId
        const hostUserId = event.playerUser.userId
        const hostHandle = event.playerUser.handle
        const game = await this.storage.getModel(new Game({gameId, hostUserId}))
        if((game.hostUserId && game.hostUserId !== hostUserId) && (hostHandle != 'maxpower')){
            throw new Error(`Game can not be started by nonhost, host was ${game.hostUserId} vs user ${hostUserId}`)
        }
        game.status = GameStatus.InProgress
        await this.storage.putModel<Game>(game)
        await this.broadcastGameState(game)
    }
    async joinGame(event: GameEvent){
        const gameId = event.gameId
        const userId = event.playerUser.userId
        const handle = event.playerUser.handle
        const game = await this.storage.getModel(new Game({gameId}))
        if(game.status && game.status !== GameStatus.Ready){
            throw new Error('Game can only be joined in Ready status')
        }
        game.players.push(new GamePlayer({userId, handle}))
        await this.storage.putModel<Game>(game)
        await this.broadcastGameState(game)
    }
    async winGame(event: WinThisGameEvent){
        const gameId = event.gameId
        const winnerUserId = event.playerUser.userId
        const game = await this.storage.getModel(new Game({gameId}))
        if(game.status !== GameStatus.InProgress){
            throw new Error('Game can only be won if in progress.')
        }
        game.gameState.score = game.players.map(player => Object.assign(new PlayerScore(), {
             playerId: player.userId,
             score: player.userId === winnerUserId ? 1 : 0
        }))
        game.status = GameStatus.Complete
        await this.storage.putModel<Game>(game)
        await this.broadcastGameState(game)
    }

    async exitGame(event: GameEvent){
        const gameId = event.gameId
        const exitingUserId = event.playerUser.userId
        const game = await this.storage.getModel(new Game({gameId}))
        const toExit = game.players.find(player => player.userId === exitingUserId)
        toExit && (toExit.didExit = true)
        const stillPlaying = game.players.filter(player => !player.didExit)
        if(stillPlaying.length === 0){
            game.status = GameStatus.Abandoned
        }
        await this.storage.putModel<Game>(game)
        await this.broadcastGameState(game)
    }
    async deleteGame(game: Game){
        await this.storage.deleteModels<Game>([game])
        // TODO this should be handled via async message platform
        await this.storage.deleteModels<ChatRoom>([this.buildGameChatRoom(game.gameId)])
    }

    async getReadyGameList(startFrom?: Game, pageSize: number = 20): Promise<GameList> {
        const startKey = startFrom || Object.assign(new Game(), { pk: '', sk: '', status: GameStatus.Ready, createDate: new Date(0) })
        const results = await this.storage.getModelsByProjectionIndex(
            startKey,
            this.indexes.StatusCreateDate,
            pageSize)
        return {
            games: results,
            lastId: results[results.length-1]?.gameId,
            pageSize
        }
    }

    async getGame(game: Game){
        return this.storage.getModel<Game>(game)
    }

    private async broadcastGameState(game: Game){
        const gamePayload = { channel: defaultChannels.game, data: game } as Payload
        const userIds = game.players
            .filter(player => ! player.computerPlayerId)
            .map(player => player.userId)
        await this.broadcaster.postMessageToUsers(gamePayload, userIds)
    }

    private buildGameChatRoom(gameId: string, hostUserId?: string){
        return new ChatRoom({
            chatRoomId: gameId,
            channelName: 'game chat',
            ownerUserId: hostUserId,
            members: []
        })
    }
}
