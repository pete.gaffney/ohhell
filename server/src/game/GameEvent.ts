import { v4 } from "uuid"
import { User } from "../user/User"

export enum GameRuleSets {
    Base = 'Base',
    WinThisGame = 'Win This Game'
}

export enum BaseGameEventTypes {
    Create = 'CREATE',
    Start = 'START',
    Join = 'JOIN',
    Exit = 'EXIT',
    Error = 'ERROR'
}
export enum WinGameEventTypes {
    Win = 'WIN'
}

export interface IGameEvent {
    eventId: string
    gameId: string
    gameRuleSetName: string
    eventType: BaseGameEventTypes | string
    playerUser: User
    eventData: any
}

export class GameEvent implements IGameEvent {
    constructor(options?: {eventId?: string, gameId?: string, gameRuleSetName: string, eventType: BaseGameEventTypes | string, playerUser: User, eventData?: any}){
        this.eventId = options?.eventId || v4()
        this.gameId = options?.gameId || ''
        this.gameRuleSetName = GameRuleSets.Base
        this.eventType = options?.eventType || BaseGameEventTypes.Error
        this.playerUser = options?.playerUser ? new User(options.playerUser) : new User()
        this.eventData = options?.eventData
    }
    eventId: string
    gameId: string
    gameRuleSetName: string
    eventType: BaseGameEventTypes | string
    playerUser: User
    eventData: any
}

export class WinThisGameEvent extends GameEvent implements IGameEvent {
    constructor(options: {eventId?: string, gameId:string, eventType: BaseGameEventTypes | WinGameEventTypes, gameRuleSetName: string, playerUser: User, eventData?: any}){
        super(options)
        this.eventType = options?.eventType || BaseGameEventTypes.Error
    }
    gameRuleSetName: string = GameRuleSets.WinThisGame
    eventType: BaseGameEventTypes | WinGameEventTypes
}