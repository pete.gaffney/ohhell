import { Module } from '@nestjs/common';
import { GameController } from './Game.controller';
import { GameService } from './GameService';
import { GameAdapter, GamePlayerAdapter } from '../storageAdapters/gameAdapters';
import { DynamoDbStorage } from '../lib/storage/storage';
import { getDynamoDBClient } from  '../lib/storage/dynamoDBClientFactory';
import { Broadcaster } from '../lib/broadcaster/Broadcaster';
import { getApiGatewayManagementApi } from '../lib/broadcaster/apiGatewayClientFactory';
import config from '../serverConfig'
import { UserConnectionStorage } from '../lib/broadcaster/UserConnectionStorage';
import { ChatRoomAdapter } from '../storageAdapters/chatRoomAdapters';

const ddb = getDynamoDBClient()

const StorageProvider = {
  provide: 'STORAGE',
  useFactory: () => {
    return new DynamoDbStorage(
      ddb,
      [GameAdapter, GamePlayerAdapter, ChatRoomAdapter],
      config.tableName
    )
  }
}

const BroadcasterProvider = {
  provide: 'BROADCASTER'.toUpperCase(),
  useFactory: () => {
    return new Broadcaster(
      getApiGatewayManagementApi(),
      new UserConnectionStorage(ddb)
    )
  }
};

@Module({
  imports: [],
  controllers: [GameController],
  providers: [GameService, StorageProvider, BroadcasterProvider],
})
export class GameModule {
  
}