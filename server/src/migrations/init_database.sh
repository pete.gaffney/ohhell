#!/bin/bash

aws dynamodb create-table --cli-input-json file://000_create_ohell_dev.json --endpoint-url http://localhost:8000
aws dynamodb batch-write-item --request-items file://001_initialseed.json --endpoint-url http://localhost:8000
aws dynamodb update-table --cli-input-json file://002_create_GSI1.json --endpoint-url http://localhost:8000
