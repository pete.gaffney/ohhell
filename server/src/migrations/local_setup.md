export AWS_ACCESS_KEY_ID='DUMMYIDEXAMPLE'
export AWS_SECRET_ACCESS_KEY='DUMMYEXAMPLEKEY'
export REGION='us-east-1'

docker run -p 8000:8000 amazon/dynamodb-local

aws dynamodb list-tables --endpoint-url http://localhost:8000

aws dynamodb create-table --cli-input-json file://create_ohell_dev.json --endpoint-url http://localhost:8000
aws dynamodb batch-write-item --request-items file://000_initialseed.json --endpoint-url http://localhost:8000

aws dynamodb scan --table-name ohell_dev --endpoint-url http://localhost:8000
