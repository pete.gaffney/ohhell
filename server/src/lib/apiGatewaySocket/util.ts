import { APIGatewayEvent } from 'aws-lambda'

export type WebsocketPayload = {
    action: string
    channel: string
    data: any
    sent: Date
}

export enum defaultChannels {
    global = 'GLOBAL',
    echo = 'ECHO',
    chat = 'CHAT',
    auth = 'AUTH',
    game = 'GAME'
}


export function getConnectionId(event: APIGatewayEvent){
    if(!event.requestContext.connectionId){
        throw new Error('API Gateway event had no websocket connection id')
    }
    return event.requestContext.connectionId
}

export function getEventBodyData(event: APIGatewayEvent){
    if(!event.body) {
        return { data: undefined }
    }
    return JSON.parse(event.body) as WebsocketPayload
}