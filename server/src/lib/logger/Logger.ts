import config from '../../serverConfig'

export class Logger {
    /* istanbul ignore next */
    static info(message: string){
        config.debug && console.info(message)
    }
    /* istanbul ignore next */
    static debug(message: string){
        config.debug && console.info(message)
    }
    /* istanbul ignore next */
    static warn(message: string){
        !config.suppressErrorLog && console.warn(message)
    }
    /* istanbul ignore next */
    static error(message: string){
        !config.suppressErrorLog && console.error(message)
    }
}