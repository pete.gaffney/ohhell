import { DynamoDbStorage } from './storage'
import { DynamoDB } from 'aws-sdk'
import { globalChatRooms } from '../../chat/ChatRoom'
import { StorageAdapters } from '../../storageAdapters'

const front = globalChatRooms.front
const frontKey = { pk: front.chatRoomId, sk: front.channelName }

const exampleGetChatRoomOutput = {
    Item: {
        members: [{wscn: 'x', uid: null },
            {wscn: 'y', uid: null },
            {wscn: 'z', uid: null }
        ]
    }
}

const exampleConnections = ['x','y','z']
const mockQuery = jest.fn()
const mockDelete = jest.fn()
const mockGet = jest.fn()
const mockPut = jest.fn()
const mockDynamoClient = {
    query: mockQuery,
    delete: mockDelete,
    get: mockGet,
    put: mockPut
} as unknown as DynamoDB.DocumentClient

function storageInit() {
    return new DynamoDbStorage(mockDynamoClient, StorageAdapters)
}



describe('DynamoDbStorage', () => {
    beforeEach(() => {
        jest.clearAllMocks()
        mockDelete.mockClear()
    })
    it('getRecord should throw exception if get does not return object with Item', async () => {
        ;(mockDynamoClient.get as jest.Mock).mockImplementation(() => ({
            promise: () => Promise.resolve({})
        }))
        const storage = storageInit()
        await expect(storage.getRawByKey(frontKey))
            .rejects.toThrowError('error getRecord - could not find')
    })
    it('getRecords should throw exception if get does not return object with Items', async () => {
        ;(mockDynamoClient.query as jest.Mock).mockImplementation(() => ({
            promise: () => Promise.resolve({})
        }))
        const storage = storageInit()
        await expect(storage.getRawsByPartitionKey(frontKey))
            .rejects.toThrowError('error getRecords - no Items returned')
    })
    it.todo('getModelsByProjectionIndex queries from named global secondary index')
    // it('should call delete for each passed connectionId', async () => {})
    // it('should throw one exception if one or more delete attempt fails', async() => {})
})
