import { toDualKeyFieldEntity, toSingleKeyFieldEntity, toPartitionKey, toStorageKey, toBaseModel } from './adapterFunctions'
import { AdapterSpec } from './adapterTypes'
import each from 'jest-each'

class Example {
    constructor() {
    } 
    key1: string = ''
    key2: string = ''
    data: string = ''
}

class SingleKeyExample {
    constructor() {
    } 
    key1: string = ''
    data: string = ''
}

const exampleSpec: AdapterSpec = {
    targetClass: Example,
    pkFieldName: 'key1',
    skFieldName: 'key2',
    kindValue: 'Example',
    indexes: {}
}

const invalidPkExampleSpec = { ...exampleSpec, pkFieldName: 'keyInvalid' }
const invalidSkExampleSpec = { ...exampleSpec, skFieldName: 'keyInvalid' }
const invalidKindExampleSpec = { ... exampleSpec, kindValue: '' }

const singleKeyExampleSpec: AdapterSpec = {
    targetClass: Example,
    pkFieldName: 'key1',
    kindValue: 'Example',
    indexes: {}
}

const myExample = Object.assign(new Example(), { key1: 'a', key2: 'b', data: 'myData' })
const myStorableExample = { pk: 'a', sk: 'b', Kind: 'Example', data: 'myData'}
const myExampleStorageKey = { pk: 'a', sk: 'b'}
const myExamplePartitionKey = { pk: 'a'}

const mySingleKeyExample = Object.assign(new SingleKeyExample(), {key1:'a', data:'myData'})
const mySingleKeyStorableExample = { pk: 'a', sk: 'a', Kind: 'Example', data: 'myData'}
const myExampleSingleStorageKey = { pk: 'a', sk: 'a'}

describe('toDualKeyFieldEntity', () => {
    it('should convert a class to an entity', () => {
        const result = toDualKeyFieldEntity(myExample, exampleSpec)
        expect(result).toEqual(myStorableExample)
    })
    it('should throw if pkFieldName does not exist on passed item', () => {
        const err = `Config Error: could not store item due to missing pkFieldName in ${
            JSON.stringify({pkFieldName: 'keyInvalid', kindValue: 'Example'})}`
        expect(() => toDualKeyFieldEntity(myExample, invalidPkExampleSpec)).toThrowError(err)
    })
    it('should throw if skFieldName does not exist on passed item', () => {
        const err = `Config Error: could not store item due to missing skFieldName in ${
            JSON.stringify({pkFieldName: 'key1', skFieldName: 'keyInvalid', kindValue: 'Example'})}`
        expect(() => toDualKeyFieldEntity(myExample, invalidSkExampleSpec)).toThrowError(err)
    })
    it('should throw if kindValue is falsey', () => {
        const err = `Config Error: could not store item due to missing kindValue`
        expect(() => toDualKeyFieldEntity(myExample, invalidKindExampleSpec)).toThrowError(err)
    })
})


describe('toSingleKeyFieldEntity', () => {
    it('should convert a class to an entity with same value in pk and sk', () => {
        const result = toSingleKeyFieldEntity(mySingleKeyExample, singleKeyExampleSpec)
        expect(result).toEqual(mySingleKeyStorableExample)
    })
    each([
        ['invalid key', 'keyInvalid'],
        ['empty string', '']
    ]).it('should throw if pkFieldName %s does not exist on passed item', (text:string, keyValue:string) => {
        const err = `Config Error: could not store item due to missing pkFieldName in ${
            JSON.stringify({pkFieldName: keyValue, kindValue: 'Example'})}`
        expect(() => toSingleKeyFieldEntity(myExample, { ...singleKeyExampleSpec, pkFieldName: keyValue })).toThrowError(err)
    })
    it('should throw if kindValue is falsey', () => {
        const err = `Config Error: could not store item due to missing kindValue`
        expect(() => toSingleKeyFieldEntity(myExample, { ...singleKeyExampleSpec, kindValue: ''})).toThrowError(err)
    })
})

describe('toBaseModel', () => {
    it('should convert named keys to corresponding class values and return a class instance', () => {
        const result = toBaseModel<Example>(myStorableExample, exampleSpec)
        expect(result).toBeInstanceOf(Example)
        expect(result).toEqual(myExample)
    })
})

describe('toStorageKeys', () => {
    it('should make StorageKeys for a class', () => {
        const result = toStorageKey(myExample, exampleSpec)
        expect(result).toEqual(myExampleStorageKey)

    })
    each([['undefined', undefined],
        ['empty string','']
    ])
    .it(`should make single key StorageKeys for a class with 2nd key '%s'`, (text: string, key2: string) => {
        const result = toStorageKey(mySingleKeyExample, { ...exampleSpec, skFieldName: key2 })
        expect(result).toEqual(myExampleSingleStorageKey) 
    })
})

describe('toPartitionKey', () => {
    it('should make PartitionKeys for a class', () => {
        const result = toPartitionKey(myExample, exampleSpec)
        expect(result).toEqual(myExamplePartitionKey)
    })
})