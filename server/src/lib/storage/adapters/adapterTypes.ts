import { Entity, StorageKey } from "../storageTypes"

export interface IndexQuerySpec {
    indexName: string,
    partitionKeyName: string,
    sortKeyName: string,
    indexKeyGenerator: (self:any) => StorageKey
}

export interface AdapterSpec {
    targetClass: new (options? : any) => any
    pkFieldName: string,
    skFieldName?: string,
    kindValue: string,
    indexes: Record<string, IndexQuerySpec>
}

export interface AdapterSet<T> {
    spec: AdapterSpec
    toEntity: (model: T) => Entity[],
    toEntities: (models: T[]) => Entity[],
    toModel: (entities: Entity[]) => T,
    toModels: (entities: Entity[]) => T[],
}
