import { AdapterSpec, IndexQuerySpec } from "./adapterTypes"
import { Entity } from "../storageTypes"
import { PartitionStorageKey, StorageKey } from "../storageTypes"

export function getNameForItemClass(item: any): string{
    const name = item?.constructor?.name
    return name || (() => { throw new Error(`attempted to fetch adapter for non-class ${JSON.stringify(item)}`) })()
}

export function getNameForClass(targetClass: any): string{
    const name = targetClass?.name
    return name || (() => { throw new Error(`attempted to fetch adapter for non-class ${JSON.stringify(targetClass)}`) })()
}

export function toSingleKeyFieldEntity (item: Record<string, any>, spec: AdapterSpec):Entity {
    const {pkFieldName, kindValue } = singleKeyAdapterFunctionCheckArgs(spec.pkFieldName, spec.kindValue, item)
    const indexValuesObject = getIndexValuesObject(item, spec)
    const entity: Entity = {
        ...item,
        ...indexValuesObject,
        pk: item[pkFieldName],
        sk: item[pkFieldName],
        Kind: kindValue, 
        [pkFieldName]: undefined}
    return entity
}

export function toDualKeyFieldEntity(item: any, spec: AdapterSpec): Entity {
    const {pkFieldName, skFieldName, kindValue } = { ...spec, skFieldName: spec.skFieldName || '' }
    const indexValuesObject = getIndexValuesObject(item, spec) 
    adapterFunctionCheckArgs(pkFieldName, skFieldName, kindValue, item)
    const entity: Entity = {
        ...item,
        ...indexValuesObject,
        pk: item[pkFieldName],
        sk: item[skFieldName],
        Kind: kindValue,
        [pkFieldName]: undefined,
        [skFieldName]: undefined}
    return entity
}

function getIndexValuesObject(item: any, spec:AdapterSpec): Record<string,any> {
    return Object.values(spec.indexes).reduce((acc: Record<string,any>, next: IndexQuerySpec) => {
        const nextKeys = next.indexKeyGenerator(item) as StorageKey
        const nextValues = {
            [next.partitionKeyName]: nextKeys.pk,
            [next.sortKeyName]: nextKeys.sk 
        } as Record<string, any>
        return { ...acc, ...nextValues }
    }, {})
}

function adapterFunctionCheckArgs(pkFieldName:string, skFieldName:string, kindValue : string, item:any) {
    singleKeyAdapterFunctionCheckArgs(pkFieldName, kindValue, item)
    if(!skFieldName || !item[skFieldName]){
        throw new Error(`Config Error: could not store item due to missing skFieldName in ${JSON.stringify({pkFieldName, skFieldName, kindValue})}` )
    }
    return { pkFieldName, skFieldName, kindValue }
}

function singleKeyAdapterFunctionCheckArgs(pkFieldName:string, kindValue : string, item:any) {
    if(!item[pkFieldName]){
        throw new Error(`Config Error: could not store item due to missing pkFieldName in ${JSON.stringify({pkFieldName, kindValue})}` )
    }
    if(!kindValue){
        throw new Error(`Config Error: could not store item due to missing kindValue`)
    }
    return { pkFieldName, kindValue }
}

export function toBaseModel<T>(entity: Entity, spec: AdapterSpec) {
    const init = new spec.targetClass()
    const newKeys = {[spec.pkFieldName]: entity.pk}
    if(spec.skFieldName){
        newKeys[spec.skFieldName] = entity.sk
    }
    const indexFieldNames = Object.values(spec.indexes).reduce(
        (fieldNames: string[], index: IndexQuerySpec ) => {
            fieldNames.push(index.partitionKeyName)
            fieldNames.push(index.sortKeyName)
            return fieldNames
        },[])
    
    const {Kind, pk, sk, ...rest} = entity
    const classMembers = rest as Record<string, any>
    indexFieldNames.forEach(fieldName => delete (classMembers)[fieldName])
    const obj = Object.assign(init, classMembers, newKeys)
    return obj as T
}

export function toStorageKey(item: any, spec: AdapterSpec): StorageKey {
    const { pkFieldName, skFieldName } = spec
    const pk = item[pkFieldName]
    const sk = (skFieldName && item[skFieldName]) || pk
    return { pk, sk }
}

export function toPartitionKey(item: any, spec: AdapterSpec): PartitionStorageKey {
    const { pkFieldName } = spec
    return { pk: item[pkFieldName] }
}
