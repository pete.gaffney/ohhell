import { DynamoDB } from 'aws-sdk'
import config from '../../serverConfig'
import { StorageKey, PartitionStorageKey, Entity, Raw } from './storageTypes'
import { convertToEntityFormat, convertToRawFormat } from './entityRawDynamoDBConverters'
import { getNameForClass, getNameForItemClass, toPartitionKey, toStorageKey } from './adapters/adapterFunctions'
import { AdapterSet, IndexQuerySpec } from './adapters/adapterTypes'
import { Injectable } from '@nestjs/common'
import { Logger } from '../logger/Logger'

export interface Storage {
    // TODO these should be broken out into a library private class
    // not doing it right now because I'd have to move around unit tests
    getRawByKey(entityKey: StorageKey): Promise<Raw>
    getRawsByPartitionKey(entityKey: PartitionStorageKey): Promise<Array<Raw>>
    putEntity(entity: Entity): Promise<void>
    deleteRawsByKeys(keys: Array<StorageKey>): Promise<void>
    getRawsByProjectionIndex(startKey: StorageKey, index: IndexQuerySpec): Promise<Array<Raw>>
    // endTODO
    getIndex(targetClass: new () => any, indexName: string): IndexQuerySpec
    getModel<T>(key: T): Promise<T>
    getModelsInBulk<T>(keys: T[]): Promise<T[]>
    getModelsByParent<T>(key: T): Promise<Array<T>>
    getModelsByProjectionIndex<T>(key: T, index: IndexQuerySpec, limit?: number): Promise<Array<T>>
    putModel<T>(record: T): Promise<void>
    deleteModels<T>(records: Array<T>): Promise<void>
}

@Injectable()
export class DynamoDbStorage implements Storage {
    ddb: DynamoDB.DocumentClient
    adapters: Record<string, AdapterSet<any>>
    defaultTableName: string
    constructor(dynamoDbClient: DynamoDB.DocumentClient, adapters: AdapterSet<any>[], defaultTableName?: string) {
        this.ddb = dynamoDbClient
        this.adapters = this.constructAdapters(adapters)
        this.defaultTableName = defaultTableName || config.tableName
    }

    public getIndex(targetClass: new () => any, indexName: string): IndexQuerySpec {
        const name = getNameForClass(targetClass)
        const indexes = this.adapters[name]?.spec?.indexes
        if(!indexes){
            throw new Error(`Configuration Error: couldn't get adapter for class ${name}`)
        }
        if(!indexes[indexName]){
            throw new Error(`Configuration Error: couldn't get index named ${indexName} for class ${name}`)
        }
        return indexes[indexName]
    }

    private constructAdapters(adapters: AdapterSet<any>[]): Record<string, AdapterSet<any>> {
        return adapters.reduce((adapters: Record<string, AdapterSet<any>>, adapter: AdapterSet<any>) => {
            adapters[getNameForClass(adapter.spec.targetClass) as string] = adapter
            return adapters
        }, {})

    }

    private getAdapterForItem<T>(item: any) {
        const name = getNameForItemClass(item)
        const adapter = this.adapters[name]
        if(!adapter){
            throw new Error(`Couldn't getAdapterForItem: ${JSON.stringify(item)} with class ${name}`)
        }
        return adapter as unknown as AdapterSet<T>
    }

    async getRawByKey(entityKey: StorageKey) {
        const getEntityParams = {
            TableName: this.defaultTableName,
            Key: entityKey
        }
        const response = await this.ddb.get(getEntityParams).promise()
        if(!response.Item){
            throw new Error(`error getRecord - could not find ${JSON.stringify(entityKey)}`)
        }
        return response.Item as Raw
    }

    //TODO tests, probably recopy updated version from UserConnection
    private async getRawBulkRecordsByKeysRecursive(remainingKeys: StorageKey[], depth: number): Promise<Array<Raw>>{
        if(depth > 8){
            throw new Error('Could not retrieve all records after 8 attempts')
        }
        const params: DynamoDB.DocumentClient.BatchGetItemInput = {
            RequestItems: {
                    [this.defaultTableName]: {
                    Keys: remainingKeys
                }
            },
            ReturnConsumedCapacity: 'NONE'
        }
        const result: DynamoDB.DocumentClient.BatchGetItemOutput = await this.ddb.batchGet(params).promise()
        const { Responses, UnprocessedKeys } = result
        const tableResponses = (Responses || {[this.defaultTableName]: []})[this.defaultTableName] as Raw[]
        const tableUnprocessedKeys = (UnprocessedKeys || {[this.defaultTableName]: []})[this.defaultTableName] as StorageKey[]
        if(tableUnprocessedKeys.length){
            await new Promise(resolve => setTimeout(resolve, 2^depth))
            return [...tableResponses, ...(await this.getRawBulkRecordsByKeysRecursive(tableUnprocessedKeys, depth + 1))]
        }
        return tableResponses
    }

    async getBulkRecordsByKeys(entityKeys: StorageKey[]) {
        return this.getRawBulkRecordsByKeysRecursive(entityKeys, 1)
    }

    async getRawsByPartitionKey(entityKey: PartitionStorageKey){
        const getEntityParams = {
            TableName: this.defaultTableName,
            KeyConditionExpression: "#pk = :pk",
            ExpressionAttributeNames:{
                "#pk": "pk"
            },
            ExpressionAttributeValues: {
                ":pk": entityKey.pk
            }
        }
        const result = await this.ddb.query(getEntityParams).promise()
        if(!result.Items){
            throw new Error(`error getRecords - no Items returned for ${JSON.stringify(entityKey)}`)
        }
        return result.Items as Array<Raw>
    }

    async putEntity(raw: Raw){
        const putParams = {
            TableName: this.defaultTableName,
            Item: raw
          }
        await this.ddb.put(putParams).promise()
    }

    async deleteRawsByKeys(keys: Array<StorageKey>){
        // TODO this is inadequate; you're supposed to use a batch write with exp backoff etc
        const conditionExpressions = keys.map(key => ({
            TableName: this.defaultTableName,
            Key: key
        }))
        await Promise.all(conditionExpressions.map(async expr => this.ddb.delete(expr).promise()))
    }
    async getRawsByProjectionIndex(startKey: StorageKey, index: IndexQuerySpec, limit: number = 20): Promise<Array<Raw>> {
        const partitionKeyName = index.partitionKeyName
        const sortKeyName = index.sortKeyName
        const input: DynamoDB.DocumentClient.QueryInput = {
            TableName: this.defaultTableName,
            IndexName: index.indexName,
            KeyConditionExpression: `#pk = :pk and #sk >= :sk`,
            ExpressionAttributeNames:{
                '#pk': partitionKeyName,
                '#sk': sortKeyName
            },
            ExpressionAttributeValues: {
                ":pk": startKey.pk ,
                ":sk": startKey.sk ,
            },
            Limit: limit
        }
        const result = await this.ddb.query(input).promise()
        if(!result.Items){
            throw new Error(`error getRecords - no Items returned for ${JSON.stringify(startKey)}`)
        }
        return result.Items as Array<Raw>
    }
    async getModel<T>(key: T): Promise<T>{
        this.throwOnPlainObject(key,'Attempted to call getModel with non-constructed object')
        const adapter = this.getAdapterForItem<T>(key)
        const storageKey = toStorageKey(key, adapter.spec)
        const raw = await this.getRawByKey(storageKey)
        if(!raw){
            throw new Error(`Could not find object for ${JSON.stringify(storageKey)}`)
        }
        const converted = convertToEntityFormat(raw) as Entity
        return adapter.toModel([converted]) as T
    }

    async getModelsInBulk<T>(keys: T[]): Promise<T[]>{
        if(keys.length = 0) {
            return []
        }
        if(keys.length > 100){
            throw new Error('Error in getModelsInBulk: bulk requests for more than 100 records at a time not supported.')
        }
        keys.map(key => this.throwOnPlainObject(key,'Attempted to call getModelsInBulk with non-constructed object'))
        const adapter = this.getAdapterForItem<T>(keys[0])
        const storageKeys = keys.map(key => toStorageKey(key, adapter.spec))
        const raws = await this.getBulkRecordsByKeys(storageKeys)
        const entityArray = raws.map(raw => convertToEntityFormat(raw) as Entity)
        return entityArray.map(entity => adapter.toModel([entity])) as T[]
    }

    async getModelsByParent<T>(key: T): Promise<Array<T>>{
        this.throwOnPlainObject(key,'Attempted to call getModelsByParent with non-constructed object')
        const adapter = this.getAdapterForItem<T>(key)
        const storageKey = toPartitionKey(key, adapter.spec)
        const raw = await this.getRawsByPartitionKey(storageKey)
        const converted = convertToEntityFormat(raw) as Entity[]
        return adapter.toModels(converted)
    }
    async getModelsByProjectionIndex<T>(model: T, index: IndexQuerySpec, limit: number = 20): Promise<Array<T>>{
        this.throwOnPlainObject(model,'Attempted to call getModelsByProjectionIndex with non-constructed object')
        const adapter = this.getAdapterForItem<T>(model)
        const storageKey = index.indexKeyGenerator(model)
        const raws = await this.getRawsByProjectionIndex(storageKey, index, limit )
        const entityArray = raws.map(raw => convertToEntityFormat(raw) as Entity)
        return entityArray.map(entity => adapter.toModel([entity])) as T[]
    }
    async putModel<T>(model: T): Promise<void>{
        this.throwOnPlainObject(model,'Attempted to call putModel with non-constructed object')
        const adapter = this.getAdapterForItem<T>(model)
        const entities = adapter.toEntity(model)
        const converted = convertToRawFormat(entities)
        const items = Array.isArray(converted) ? converted : [converted]
        for (const item of items){
            await this.putEntity(item)
        }
    } 
    async deleteModels<T>(models: Array<T>): Promise<void>{
        if(models.length === 0){
            return
        }
        this.throwOnPlainObject(models,'Attempted to call deleteModels with non-constructed object')
        const adapter = this.getAdapterForItem(models[0])
        const storageKeys = models.map(record => toStorageKey(record, adapter.spec))
        this.deleteRawsByKeys(storageKeys)
    }

    private throwOnPlainObject(model: any, errorMessage: string){
        if(!model?.constructor || model?.constructor?.name === {}.constructor.name) {
            throw new Error(errorMessage)
        }
    }
}
