import { convertToRawFormat, convertToEntityFormat } from './entityRawDynamoDBConverters'

class DoubleEmbedded {
    data: string = ''
    deDate: Date = new Date(0)
}

class Embedded {
    embeddedId: string = ''
    data: string = ''
    embeddedDate: Date = new Date(0)
    array: Array<DoubleEmbedded> = []
}

const entityExample = {
    pk: 'thisid',
    sk: 'thisid',
    Kind: 'ModelExample',
    id: 'thisid',
    data: { innerData: 'some data string' },
    myDate: 1633068061001 ,
    embedded: {
        embeddedId: 'asdf',
        data: 'this that',
        embeddedDate: 1633068061002,
        array: [
            {
                data: 'data1',
                deDate: 1633068061003
            },
            {
                data: 'data2',
                deDate: 1633068061004
            }
        ]
    }
}

const modelExample = {
    pk: 'thisid',
    sk: 'thisid',
    Kind: 'ModelExample',
    id: 'thisid',
    data: { innerData: 'some data string' },
    myDate: new Date(1633068061001),
    embedded: Object.assign(new Embedded(),
        {
            embeddedId: 'asdf',
            data: 'this that',
            embeddedDate: new Date(1633068061002),
            array: [
                Object.assign(new DoubleEmbedded(), {
                    data: 'data1',
                    deDate: new Date(1633068061003)
                }),
                Object.assign(new DoubleEmbedded(), {
                    data: 'data2',
                    deDate: new Date(1633068061004)
                })
            ]
        })
}

const entityArrayExample = [
    {
        pk: 'a',
        sk: 'a',
        Kind: 'other',
        data: 'mydata1',
        myDate: 1633068061001
    },
    {
        pk: 'b',
        sk: 'b',
        Kind: 'other',
        data: 'mydata1',
        myDate2: 1633068061002
    }
]

const modelArrayExample = [
    {
        pk: 'a',
        sk: 'a',
        Kind: 'other',
        data: 'mydata1',
        myDate: new Date(1633068061001)
    },
    {
        pk: 'b',
        sk: 'b',
        Kind: 'other',
        data: 'mydata1',
        myDate2: new Date(1633068061002)
    }
]

describe('convertToEntityFormat', () => {
    it('should convert dates to number entity format', () => {
        expect(convertToRawFormat(modelExample)).toEqual(entityExample)
    })
    it('should convert dates to number format for array of models', () => {
        expect(convertToRawFormat(modelArrayExample)).toEqual(entityArrayExample)
    })
})

describe('convertToModelFormat', () => {
    it('should convert dates to date object format', () => {
        expect(convertToEntityFormat(entityExample)).toEqual(modelExample)
    })
    it('should convert dates to date object format for array of entities', () => {
        expect(convertToEntityFormat(entityArrayExample)).toEqual(modelArrayExample)
    })
})