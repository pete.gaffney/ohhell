import { Entity, Raw } from './storageTypes'

function dateToNumber(key: string, value: Date | any): number | any {
    if(typeof value === 'object' && value instanceof Date){
        if(!key.includes('Date')){
            throw new Error(`Found Date object at non-date-named key ${key}`)
        }
        return value.valueOf()
    }
    return value
}

function numberToDate(key: string, value: number | any): Date | any {
    if(typeof value === 'number' && key.includes('Date')){
        return new Date(value)
    }
    return value
}

function mustConvertFurther(value: any): boolean{
    return typeof value === 'object'
}

export function convertToRawFormat(entity:(Entity | Array<Entity>)): Raw | Array<Raw> {
    if(entity == null){
        return entity
    }
    if(Array.isArray(entity)){
        return entity.map(item => convertToRawFormat(item)) as Entity[]
    }
    const result = Object.entries(entity).reduce((acc : any, next) => {
        const nextKey = next[0]
        const nextValue = dateToNumber(nextKey, next[1])
        acc[nextKey] = mustConvertFurther(nextValue)
            ? convertToRawFormat(nextValue)
            : nextValue
        return acc
    }, {}) as unknown as Raw
    return result
}

export function convertToEntityFormat(entity: Raw | Array<Raw>): Entity | Array<Entity> {
    return convertAnyToEntityFormat(entity)
}

function convertAnyToEntityFormat(entity: any): Entity | Array<Entity> {
    if(entity == null){
        return entity
    }
    if(Array.isArray(entity)){
        return entity.map(item => convertAnyToEntityFormat(item)) as Entity[]
    }
    return Object.entries(entity).reduce((acc : any, next) => {
        const nextKey = next[0]
        const nextValue = mustConvertFurther(next[1])
            ? convertAnyToEntityFormat(next[1])
            : next[1]
        acc[nextKey] = numberToDate(nextKey, nextValue)
        return acc
    }, {}) as unknown as Entity
}
