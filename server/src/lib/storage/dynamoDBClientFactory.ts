import { DynamoDB } from 'aws-sdk'

import config from '../../serverConfig'
import appConstants from '../../appConstants'
import { Logger } from '../../lib/logger/Logger'

export function getDynamoDBClient():DynamoDB.DocumentClient {
    const localOptions = config.isLocal
        ? { endpoint: config.dynamoEndpoint }
        : {}
    const options = {
        apiVersion: appConstants.dynamoApiVersion,
        region: config.awsRegion,
        ...localOptions
    }
    const client = new DynamoDB.DocumentClient()
    return client
}
