// TODO authentication

export type AuthTicket = {
    userId: string,
    handle: string
}

export function getUserIdInsecure(authTicket: AuthTicket){
    if(!authTicket?.userId){
        throw new Error(`authTicket missing or had no userId: ${authTicket}`)
    }
    return { userId: authTicket.userId, handle: authTicket.handle }
}