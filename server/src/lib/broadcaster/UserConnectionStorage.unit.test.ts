import { DynamoDB } from 'aws-sdk'
import config from '../../serverConfig'
import { Connection, ConnectionList, UserConnectionStorage } from './UserConnectionStorage'

const exampleConnectionList : ConnectionList = {
    userId: 'thisperson',
    connections: ['cn1','cn2','cn3']
}

const rawConnectionList = {
    pk: exampleConnectionList.userId,
    sk: 'UserConnection',
    userId: exampleConnectionList.userId,
    connections: exampleConnectionList.connections,
    Kind: 'UserConnection'
}

const mockDynamoClient = {
    query: jest.fn(),
    delete: jest.fn(),
    get: jest.fn(),
    put: jest.fn()
} as unknown as DynamoDB.DocumentClient

const examplePostToConnection = {
    promise: async () => Promise.resolve()
}
const examplePostToConnection410 = {
    promise: async () => Promise.reject({statusCode: 410})
}
const examplePostToConnection500 = {
    promise: async () => Promise.reject({statusCode: 500})
}

describe.skip('UserConnectionStorage getConnectionLists', () => {

})

describe('UserConnectionStorage storeConnectionList', () => {
    it('should put submitted connection list in raw form to Dynamo', async () => {
        ;(mockDynamoClient.put as jest.Mock).mockImplementation(() => ({
            promise: () => Promise.resolve()
        }))
        const svc = new UserConnectionStorage(mockDynamoClient)
        await expect(svc.storeConnectionList(exampleConnectionList)).resolves.toBeFalsy()
        expect(mockDynamoClient.put).toHaveBeenCalledWith({ Item: rawConnectionList, TableName: config.tableName })
    })
})

describe.skip('UserConnectionStorage removeConnections', () => {
// should remove specified connections
// should delete user connections if connections array empty
})


    // it('should return failedConnections: [] for happy path run', async () => {
    //     mockPostToConnection.mockImplementation(() => examplePostToConnection)
    //     const broadcaster = new Broadcaster(mockApiGatewayClient, mockDynamoClient)
    //     const result = await broadcaster.postMessageToActiveUsers(exampleData, exampleUserConnections)
    //     expect(result).toEqual(exampleGoodResults)
    // })
    // it('should return connectionIds for each connection that returns a 410', async () => { 
    //     mockPostToConnection.mockImplementation(
    //         (params: ApiGatewayManagementApi.PostToConnectionRequest) => {
    //             if(['a','c'].includes(params.ConnectionId)){
    //                 return examplePostToConnection
    //             }
    //             return examplePostToConnection410
    //     })
    //     const broadcaster = new Broadcaster(mockApiGatewayClient, mockDynamoClient)
    //     const result = await broadcaster.postMessageToActiveUsers(exampleData, exampleUserConnections)
    //     expect(result).toEqual({ failedConnections:['b','d'] })
    // })
    // it('should throw single exception if connections are failing to transmit', async () => {
    //     mockPostToConnection.mockImplementation(
    //         (params: ApiGatewayManagementApi.PostToConnectionRequest) => {
    //             if(['a','c'].includes(params.ConnectionId)){
    //                 return examplePostToConnection
    //             }
    //             return examplePostToConnection500
    //     })
    //     const broadcaster = new Broadcaster(mockApiGatewayClient, mockDynamoClient)
    //     await expect(broadcaster.postMessageToActiveUsers(exampleData, exampleUserConnections))
    //         .rejects.toMatchObject({ statusCode: 500})
    // })

    