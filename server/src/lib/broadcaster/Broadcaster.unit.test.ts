import { Broadcaster } from './Broadcaster'
import { ApiGatewayManagementApi, DynamoDB } from 'aws-sdk'
import config from '../../serverConfig'
import { UserConnectionStorage, ConnectionList, Connection } from './UserConnectionStorage'
import { globalChatRooms } from '../../chat/ChatRoom'
import { ChatData } from '../../chat/ChatService'
import { Pronouns } from '../storage/storageTypes'
import { defaultChannels } from '../../lib/apiGatewaySocket/util'


const exampleText = 'example text'
const exampleChatData: ChatData = {
    text: exampleText,
    fromMember: { uid: 'from user id', handle: 'from handle', pronouns: Pronouns.They, lastActivityDate: new Date() },
    toChatRoomId: globalChatRooms.front.chatRoomId,
    toChatRoomName: globalChatRooms.front.channelName,
    timeOrigin: new Date()
}
const examplePayload = {
    channel: defaultChannels.chat,
    data: exampleChatData
}

const exampleConnectionLists: ConnectionList[] = [
    { userId: 'usera', connections: ['a1','a2'] },
    { userId: 'userb', connections: ['b1'] },
    { userId: 'userc', connections: ['c1', 'c2','c3'] },
    { userId: 'userd', connections: ['d'] },
    { userId: 'userd', connections: ['d'] }

]

const proposed410Connections = ['a2', 'c2']
const proposed500Connections = ['c2']


const exampleUserIds = exampleConnectionLists.map(userConnection => userConnection.userId)

const mockApiGatewayClient = {
    postToConnection: jest.fn()
} as unknown as ApiGatewayManagementApi

const mockUserConnectionStorage = {
    getConnectionLists: jest.fn(),
    storeConnectionList: jest.fn(),
    removeFailedConnections: jest.fn()
} as unknown as UserConnectionStorage

describe('broadcaster registerUserConnection',() => {
    beforeEach(() => {
        jest.clearAllMocks()
    })
    it('should check for existing user', async () => {
        const broadcaster = new Broadcaster(mockApiGatewayClient, mockUserConnectionStorage)
        const uc = exampleConnectionLists[0]
        await expect(broadcaster.registerUserConnection(uc.userId, uc.connections[0])).resolves.toBeFalsy()
        expect(mockUserConnectionStorage.getConnectionLists).toHaveBeenCalledWith([uc.userId])
    })
    it('should store new userConnectionList if nonexistent', async () => {
        const broadcaster = new Broadcaster(mockApiGatewayClient, mockUserConnectionStorage)
        const uc = exampleConnectionLists[0]
        ;(mockUserConnectionStorage.getConnectionLists as jest.Mock).mockImplementation(() => [])
        await broadcaster.registerUserConnection(uc.userId, uc.connections[0])
        expect(mockUserConnectionStorage.storeConnectionList).toHaveBeenCalledWith({ userId: uc.userId, connections: [uc.connections[0]] })
    })
    it('should add to existing userConnectionList if found', async () => {
        const broadcaster = new Broadcaster(mockApiGatewayClient, mockUserConnectionStorage)
        const uc = exampleConnectionLists[0]
        const next = 'z'
        ;(mockUserConnectionStorage.getConnectionLists as jest.Mock).mockImplementation(() => [uc])
        const expectedConnectionList = { userId: uc.userId, connections: [...uc.connections, next]}
        await broadcaster.registerUserConnection(uc.userId, next)
        expect(mockUserConnectionStorage.storeConnectionList).toHaveBeenCalledWith(expectedConnectionList)
    })
})

describe('broadcaster unregisterUserConnection',() => {
    beforeEach(() => {
        jest.clearAllMocks()
    })
    it.skip('should get existing user', async () => {
        const broadcaster = new Broadcaster(mockApiGatewayClient, mockUserConnectionStorage)
        const uc = exampleConnectionLists[0]
        await expect(broadcaster.unregisterUserConnection(uc.userId, uc.connections[0])).resolves.toBeFalsy()
        expect(mockUserConnectionStorage.getConnectionLists).toHaveBeenCalledWith([uc.userId])
    })
    it.skip('should return if existing user not found', async () => {
        const broadcaster = new Broadcaster(mockApiGatewayClient, mockUserConnectionStorage)
        const uc = exampleConnectionLists[0]
        ;(mockUserConnectionStorage.getConnectionLists as jest.Mock).mockImplementationOnce(() => [])
        await expect(broadcaster.unregisterUserConnection(uc.userId, uc.connections[0])).resolves.toBeFalsy()
        expect(mockUserConnectionStorage.getConnectionLists).toHaveBeenCalledWith([uc.userId])
        ;(mockUserConnectionStorage.getConnectionLists as jest.Mock).mockImplementationOnce(() => undefined)
        await expect(broadcaster.unregisterUserConnection(uc.userId, uc.connections[0])).resolves.toBeFalsy()
        expect(mockUserConnectionStorage.getConnectionLists).toHaveBeenCalledWith([uc.userId])
    })
    it.skip('should call storeConnectionList with reduced connection list if found', async () => {
        const broadcaster = new Broadcaster(mockApiGatewayClient, mockUserConnectionStorage)
        const uc = exampleConnectionLists[0]
        const next = 'z'
        const connectionList = { userId: uc.userId, connections: [...uc.connections, next]}
        ;(mockUserConnectionStorage.getConnectionLists as jest.Mock).mockImplementation(() => [connectionList])
        const expectedConnectionList = { userId: uc.userId, connections: [...uc.connections]}
        await broadcaster.unregisterUserConnection(uc.userId, next)
        expect(mockUserConnectionStorage.storeConnectionList).toHaveBeenCalledWith(expectedConnectionList)
    })
})


describe('broadcaster postMessageToUsers',() => {
    beforeEach(() => {
        jest.clearAllMocks()
        ;(mockApiGatewayClient.postToConnection as jest.Mock).mockImplementation(() => ({
            promise: () => Promise.resolve({})
        }))
    })
    it('should get list of connections', async () => {
        ;(mockUserConnectionStorage.getConnectionLists as jest.Mock).mockImplementation(() => exampleConnectionLists)
        // ;(mockUserConnectionStorage.removeFailedConnections as jest.Mock).mockImplementation()
        const broadcaster = new Broadcaster(mockApiGatewayClient, mockUserConnectionStorage)
        const result = await broadcaster.postMessageToUsers(examplePayload, exampleUserIds)
        expect(mockUserConnectionStorage.getConnectionLists).toHaveBeenCalledTimes(1)
    })

    it('should call apiGatewayManagementApi with data for each unique user found', async () => {
        ;(mockUserConnectionStorage.getConnectionLists as jest.Mock).mockImplementation(() => exampleConnectionLists)
        const broadcaster = new Broadcaster(mockApiGatewayClient, mockUserConnectionStorage)
        const expectedCalls = exampleConnectionLists.reduce((acc: Connection[], next: ConnectionList) => {
            const nextConnections = Array.from(new Set(next.connections))
            const userConnections = nextConnections.map(connection => ({ userId: next.userId, connection })) as Connection[]
            return acc.concat(userConnections)
        }, [])
        await broadcaster.postMessageToUsers(examplePayload, exampleUserIds)
        expect(mockApiGatewayClient.postToConnection).toHaveBeenCalledTimes(expectedCalls.length)
        expectedCalls.forEach((call) => {
            expect(mockApiGatewayClient.postToConnection).toHaveBeenCalledWith({ ConnectionId: call.connection, Data: JSON.stringify(examplePayload) })
        })
    })
})