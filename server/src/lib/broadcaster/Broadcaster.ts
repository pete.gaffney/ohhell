import { Injectable } from '@nestjs/common'
import { ApiGatewayManagementApi, AWSError } from 'aws-sdk'
import { connection } from 'websocket'
import { Logger } from '../logger/Logger'
import { UserConnectionStorage, ConnectionList, Connection } from './UserConnectionStorage'

export type Payload = {
    channel: string
    data: any
}

@Injectable()
export class Broadcaster {
    private apiGatewayManagementApi: ApiGatewayManagementApi
    private connectionStorage : UserConnectionStorage
    constructor(apiGatewayManagementApi: ApiGatewayManagementApi, connectionStorage: UserConnectionStorage ) {
        this.apiGatewayManagementApi = apiGatewayManagementApi
        this.connectionStorage = connectionStorage
    }

    async postMessageToUsers(payload: Payload, userIds: Array<string>): Promise<void> {
        Logger.debug(`Broadcaster postMessageToUsers: ${JSON.stringify(payload)} for ${JSON.stringify(userIds)}`)
        const stringifiedMessage = this.getStringifiedMessage(payload)
        const deduplicatedUsers = Array.from(new Set(userIds))
        const connectionLists = await this.connectionStorage.getConnectionLists(deduplicatedUsers)
        Logger.debug(`Broadcaster postMessageToUsers got these connectionLists: ${JSON.stringify(connectionLists)}`)

        const postCalls = connectionLists.map(async (connectionList: ConnectionList) => 
            this.postToConnectionList(stringifiedMessage, connectionList)
        )
        await Promise.all(postCalls)
    }

    async registerUserConnection(userId: string, connectionId: string){
        Logger.debug(`registerUserConnection with ${userId} and ${connectionId}`)
        const fetched = (await this.connectionStorage.getConnectionLists([userId]))
        Logger.debug(`registerUserConnection fetched ${JSON.stringify(fetched)}`) 
        const toStore = fetched && fetched[0] ? fetched[0] : { userId, connections: []}
        if(toStore.connections.includes(connectionId)){
            return
        }
        toStore.connections.push(connectionId)
        await this.connectionStorage.storeConnectionList(toStore)
        Logger.debug(`registerUserConnection stored ${JSON.stringify(toStore)}`)
    }

    async unregisterUserConnection(userId: string, connectionId: string){
        const fetched = (await this.connectionStorage.getConnectionLists([userId]))
        if(!fetched || ! fetched[0]){
            return
        }
        const connectionList = fetched[0]
        const toStore: ConnectionList = { userId: connectionList.userId, connections: connectionList.connections.filter(connection => connection !== connectionId)}
        await this.connectionStorage.storeConnectionList(toStore)
    }

    private async postToConnectionAndReturnFailedConnectionId(params: { ConnectionId: string, Data: string }){
        try {
            await this.apiGatewayManagementApi.postToConnection(params).promise();
        } catch (e) {
            const error = e as AWSError
            if (error.statusCode === 410) {
                return params.ConnectionId
            } else {
                throw new Error(error.message || `Unknown error in postToConnection status ${error?.statusCode || -1}`)
            }
        }
    }

    private async postToConnectionList(stringified: string, connectionList: ConnectionList) : Promise<void> {
        // returns connectionId if was valid connection
        Logger.debug(`postToConnectionList of ${stringified} to ${JSON.stringify(connectionList)}`)
        const failedConnections = [] as string[]
        for(let connection of connectionList.connections){
            const result = await this.postToConnectionAndReturnFailedConnectionId({ ConnectionId: connection, Data: stringified })
            if(result){
                failedConnections.push(result)
            }
        }
        Logger.debug(`sendChatHandler: postedToConnections ${JSON.stringify(connectionList.connections)}, failed connections: ${JSON.stringify(failedConnections)}`)
        if(failedConnections.length){
            this.connectionStorage.removeConnections(connectionList, failedConnections)
        }
        
    }

    private getStringifiedMessage(data: any): string {
        return (typeof data === 'string') ? data : JSON.stringify(data)
    }
}
