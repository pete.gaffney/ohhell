import { Injectable } from '@nestjs/common'
import { DynamoDB } from 'aws-sdk'
import config from '../../serverConfig'
import { Logger } from '../../lib/logger/Logger'


export type ConnectionList = {
    userId: string
    connections: string[]
}

export type Connection = {
    userId: string
    connection: string
}
export type ConnectionListKey = {
    pk: string
    sk: string
}

@Injectable()
export class UserConnectionStorage {
    // TODO move all this to a simpler implementation where we just store this stuff in cache.
    // This data does not make sense to persist between application restarts, and is 
    // unnecessarily complicated due to the need to transform it between flat and nested.
    constructor(ddb: DynamoDB.DocumentClient){
        this.ddb = ddb
    }
    private ddb: DynamoDB.DocumentClient

    async getConnectionLists( userIds: Array<string>): Promise<ConnectionList[]> {
        const keys = userIds.map(userId => this.connectionListToKey({userId}))
        const chunksOf100Keys: ConnectionListKey[][] = []
        for(let i = 0; i < keys.length; i += 100 ){
             chunksOf100Keys.push(keys.slice(i, i + 100))
        }

        const results = await chunksOf100Keys.reduce(async (resultsPromise: Promise<ConnectionList[]>, chunk: ConnectionListKey[]) => {
            const results: ConnectionList[] = await resultsPromise
            const chunkResults = await this.getConnectionListsRecursive(chunk)
            Logger.debug(`getConnectionLists got chunkresults ${JSON.stringify(chunkResults)}`)
            return results.concat(...chunkResults)
        }, Promise.resolve([]))
        Logger.debug(`getConnectionLists got results ${JSON.stringify(results)}`)
        return results
    }

    async storeConnectionList(connectionList: ConnectionList): Promise<void>{
        if(connectionList?.connections?.length == 0){
            return await this.deleteConnectionList(connectionList)
        }
        const putParams = {
            TableName: config.tableName,
            Item: this.connectionListToRaw(connectionList)
          }
        await this.ddb.put(putParams).promise()
    }

    async removeConnections(connectionList: ConnectionList, failedConnections: string[]){
        if(!failedConnections.length || !connectionList.connections.length){
            return
        }
        const newConnectionList = {
            userId: connectionList.userId,
            connections: connectionList.connections.filter(connection => !failedConnections.includes(connection))
        }
        await this.storeConnectionList(newConnectionList)
    }

    private async deleteConnectionList(connectionList: ConnectionList){
        const deleteParams = {
            TableName: config.tableName,
            Key: this.connectionListToKey(connectionList)
          }
        await this.ddb.delete(deleteParams).promise()
    }

    private async getConnectionListsRecursive(remainingKeys: ConnectionListKey[], depth: number = 1): Promise<Array<ConnectionList>>{
        // AWS strongly suggests you implement exponential backoff when using batchGet. Hence,
        if(depth > 8){
            Logger.debug('throwing Could not retrieve all UserConnectionLists after 8 attempts')
            throw new Error('Could not retrieve all UserConnectionLists after 8 attempts')
        }
        const params: DynamoDB.DocumentClient.BatchGetItemInput = {
            RequestItems: { [config.tableName]: { Keys: remainingKeys } },
            ReturnConsumedCapacity: 'NONE'
        }
        Logger.debug(`getConnectionListsRecursive requesting ${JSON.stringify(params)}`)
        const result = await this.ddb.batchGet(params).promise()
        if(!result) {
            throw new Error(`getConnectionListsRecursive get request falsy response`)
        }
        Logger.debug(`getConnectionListsRecursive got ${JSON.stringify(result)}`)
        const { responses, unprocessedKeys } = this.getTableSpecificBatchGetResponse(config.tableName, result)
        Logger.debug(`getConnectionListsRecursive tableSpecific: ${JSON.stringify({ responses, unprocessedKeys })}`)
        if(unprocessedKeys && unprocessedKeys.length){
            await new Promise(resolve => setTimeout(resolve, 2^depth))
            const nextResults = await this.getConnectionListsRecursive(unprocessedKeys, depth + 1)
            return [...responses, ...nextResults]
        }
        return responses
    }

    private getTableSpecificBatchGetResponse(tablename: string, response: DynamoDB.DocumentClient.BatchGetItemOutput){
        const { Responses, UnprocessedKeys } = response
        const unprocessedKeys = (UnprocessedKeys || {[tablename]: []})[tablename] as ConnectionListKey[]
        const rawResponses = (Responses || {[tablename]: []})[tablename]
        const responses = rawResponses.map(response => this.rawToConnectionList(response)) as ConnectionList[]
        return { responses, unprocessedKeys }
    }

    private get Kind() { return 'UserConnection' }
    private connectionListToKey(connection: { userId: string}) {
        return { 
            pk: connection.userId,
            sk: this.Kind 
        } as ConnectionListKey
    }

    private rawToConnectionList(raw: any) {
        return {
            userId: raw.pk,
            connections: raw.connections 
        } as ConnectionList
    }

    private connectionListToRaw(connectionList : ConnectionList) {
        return {
            ...this.connectionListToKey(connectionList),
            userId: connectionList.userId,
            Kind: this.Kind, 
            connections: connectionList.connections
        }
    }

}