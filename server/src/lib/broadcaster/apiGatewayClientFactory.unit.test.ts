import * as AWS from 'aws-sdk'
import { getApiGatewayManagementApi } from './apiGatewayClientFactory'


function getMyPath() { return 'myPath' }
function getMyGatewayVersion() { return 'myGatewayVersion' }
function getMockConfig() {
    return {
        __esModule: true,
        default: {
            apiGatewayPath: getMyPath()
        }
    }
}
function getMockAppSettings() {
    return {
        __esModule: true,
        default: {
            apiGatewayVersion: getMyGatewayVersion()
        }
    }
}
const expectedParameter = {
    apiVersion: getMyGatewayVersion(),
    endpoint: getMyPath()
}

jest.mock('../../serverConfig', () => getMockConfig())
jest.mock('../../appConstants', () => getMockAppSettings())
jest.mock('aws-sdk', ()=> {
    return {
        ApiGatewayManagementApi : jest.fn().mockImplementation(() => { return {} })
    }
});

test('AWS.ApiGatewayManagementApi is constructed', () => {
    const api = getApiGatewayManagementApi()
    expect(api).toBeTruthy()
    expect(AWS.ApiGatewayManagementApi).toHaveBeenCalledWith(expectedParameter)
});