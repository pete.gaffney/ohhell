import { ApiGatewayManagementApi } from 'aws-sdk'
import config from '../../serverConfig'
import appConstants from '../../appConstants'

const apigManagementOptions = {
    apiVersion: appConstants.apiGatewayVersion,
    endpoint: config.apiGatewayPath
}

export function getApiGatewayManagementApi():ApiGatewayManagementApi  {
    return new ApiGatewayManagementApi(apigManagementOptions)
}