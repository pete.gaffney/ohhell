
import * as lambdaFast from 'aws-lambda-fastify';
import { Context, APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { bootServer, NestApp } from './bootServer';

let cachedNestApp: NestApp;

export const handler = async (event: APIGatewayProxyEvent, context: Context,): Promise<APIGatewayProxyResult> => {
    if (!cachedNestApp) {
        cachedNestApp = await bootServer();
    }
    const proxy = lambdaFast.default(cachedNestApp.instance);
    return proxy(event, context);
};