import { Pronouns } from '../lib/storage/storageTypes'
import { Storage } from '../lib/storage/storage'
import { User } from './User'

export class UserService {
    constructor(storage: Storage){
        this.storage = storage
    }
    private storage: Storage
    async setUser(user: User) {
        this.storage.putModel<User>(user)
    }

    async getUser(user: User) {
        return this.storage.getModel<User>(user)
    }
    
    async deleteUser(user: User) {
        return this.storage.deleteModels([user])
    }
}
