
import { Module } from '@nestjs/common'
import { HealthCheckModule } from './healthcheck/HealthCheck.module'
import { GameModule } from './game/Game.module'

@Module({
  imports: [HealthCheckModule, GameModule],
})
export class AppModule {}
