import { Controller, Get } from '@nestjs/common';
import config from '../serverConfig';

@Controller('healthcheck')
export class HealthCheckController {
  @Get()
  getHealthCheck(): Record<string,string> {
    return {
        health: '⛥🍷🪄🗡️ Health!',
        current_timestamp: (new Date()).toISOString(),
        environment: config.environment
    }
  }
}