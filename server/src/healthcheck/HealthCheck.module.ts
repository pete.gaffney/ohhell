import { Module } from '@nestjs/common';
import { HealthCheckController } from './HealthCheck.controller';



@Module({
  imports: [],
  controllers: [HealthCheckController],
  providers: [],
})
export class HealthCheckModule {
  
}