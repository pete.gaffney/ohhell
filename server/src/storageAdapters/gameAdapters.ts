import { AdapterSet, AdapterSpec } from '../lib/storage/adapters/adapterTypes'
import { Game, GamePlayer, GameState } from '../game/GameService'
import { Entity } from '../lib/storage/storageTypes'
import { toSingleKeyFieldEntity, toBaseModel, getNameForClass } from '../lib/storage/adapters/adapterFunctions'

export const gameIndexes = {
    StatusCreateDate: {
        indexName: 'GSI1',
        partitionKeyName: 'gsi1_pk',
        sortKeyName: 'gsi1_sk',
        indexKeyGenerator: (game: Game) => ({ pk: game.status, sk: game.createDate.valueOf().toString()})
    }
}

const gameSpec: AdapterSpec = {
    targetClass: Game,
    pkFieldName: 'gameId',
    skFieldName: undefined,
    kindValue: 'Game',
    indexes: gameIndexes
}

const gamePlayerSpec = {
    targetClass: GamePlayer,
    pkFieldName: 'gameId',
    skFieldName: 'userId',
    kindValue: 'GamePlayer',
    indexes: {}
}

function toGameEntity(model: Game){
    const gameEntity = toSingleKeyFieldEntity(model, gameSpec)
    return [gameEntity]
}

function toGameEntities(models: Game[]){
    const nested = models.map(model => toGameEntity(model)) as Array<Entity[]>
    return nested.reduce((acc, val) => acc.concat(val), [])
}

function toGameModels(entityArray: Entity[]){
    const partitionMap = entityArray.reduce(
        (acc: Record<string, Entity[]>, next: Entity) => {
            acc[next.pk]
                ? acc[next.pk].push(next)
                : acc[next.pk] = [next]
            return acc
        }, {}
    )
    const games = Object.values(partitionMap).map(partition => toGameModel(partition))
    return games as Game[]
}

function toGameModel(entityArray: Entity[]): Game {
    const kindMap = entityArray.reduce(
        (acc: Record<string, Entity[]>, next: Entity) => {
            acc[next.Kind]
                ? acc[next.Kind].push(next)
                : acc[next.Kind] = [next]
            return acc
        }, {}
    )
    const game = toBaseModel<Game>(kindMap[Game.name][0], gameSpec)
    // const game = Object.assign(new Game(), gameEntity)
    game.players = game.players.map(player => Object.assign(new GamePlayer(), player))
    // game.players = toGamePlayerModels(kindMap[GamePlayer.name] || [])
    game.gameState = Object.assign(new GameState(), game.gameState)
    return game
}

const GameAdapter: AdapterSet<Game> = {
    spec: gameSpec,
    toEntity: toGameEntity,
    toEntities: toGameEntities,
    toModel: toGameModel,
    toModels: toGameModels
}

function toGamePlayerSingleEntity(model: GamePlayer){
    return toSingleKeyFieldEntity(model, gamePlayerSpec)
}

function toGamePlayerEntity(model: GamePlayer){
    return [toGamePlayerSingleEntity(model)]
}

function toGamePlayerEntities(models: GamePlayer[]){
    return models.map(model => toGamePlayerSingleEntity(model))
}

function toGamePlayerModels(entities: Entity[]){
    return entities
        .map(entity => toBaseModel<GamePlayer>(entity, gamePlayerSpec))
        .map(gamePlayerEntity => Object.assign(new GamePlayer(), gamePlayerEntity)) as GamePlayer[]
}

function toGamePlayerModel(entities: Entity[]){
    if(entities.length !== 1){
        throw new Error('too many entities for a single GamePlayer')
    }
    return toGamePlayerModels(entities)[0]
}

const GamePlayerAdapter: AdapterSet<GamePlayer> = {
    spec: gamePlayerSpec,
    toEntity: toGamePlayerEntity,
    toEntities: toGamePlayerEntities,
    toModel: toGamePlayerModel,
    toModels: toGamePlayerModels
}

export const GameAdapterObject = { [getNameForClass(Game)]: GameAdapter }
export const GamePlayerAdapterObject = { [getNameForClass(GamePlayer)]: GamePlayerAdapter }

export { GameAdapter, GamePlayerAdapter }