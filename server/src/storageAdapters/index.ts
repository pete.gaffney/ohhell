import { GameAdapter, GamePlayerAdapter } from './gameAdapters'
import { UserAdapter } from './userAdapters'
import { ChatRoomAdapter } from './chatRoomAdapters'
import { AdapterSet } from '../lib/storage/adapters/adapterTypes'

export const StorageAdapters: Array<AdapterSet<any>> = [
    GamePlayerAdapter,
    GameAdapter,
    UserAdapter,
    ChatRoomAdapter
]
