import { AdapterSet } from '../lib/storage/adapters/adapterTypes'
import { ChatRoom } from '../chat/ChatRoom'
import { ChatRoomMember } from '../chat/ChatRoomMember'
import { Entity } from '../lib/storage/storageTypes'
import { toDualKeyFieldEntity, toBaseModel, getNameForClass } from '../lib/storage/adapters/adapterFunctions'

const chatRoomSpec = { targetClass: ChatRoom,
    pkFieldName: 'chatRoomId',
    skFieldName: 'channelName',
    kindValue: "ChatRoom",
    indexes: {}
}

function toChatRoomEntity(model: ChatRoom){
    const chatRoomEntity = toDualKeyFieldEntity(model, chatRoomSpec)
    return [chatRoomEntity]
}

function toChatRoomEntities(models: ChatRoom[]){
    const nested = models.map(model => toChatRoomEntity(model)) as Array<Entity[]>
    return nested.reduce((acc, val) => acc.concat(val), [])
}

function toChatRoomModels(entityArray: Entity[]){
    const chatRooms = entityArray.map(entity => toChatRoomModel([entity]))
    return chatRooms as ChatRoom[]
}

function toChatRoomModel(entityArray: Entity[]): ChatRoom {
    if(entityArray.length !== 1){
        throw new Error('attempted to merge multiple entities into a single chatroom model')
    }
    const spec = chatRoomSpec
    const chatRoom = toBaseModel<ChatRoom>(entityArray[0], spec)
    chatRoom.members = chatRoom.members.map(member => Object.assign(new ChatRoomMember(), member))
    return chatRoom
}

const ChatRoomAdapter: AdapterSet<ChatRoom> = {
    spec: chatRoomSpec,
    toEntity: toChatRoomEntity,
    toEntities: toChatRoomEntities,
    toModel: toChatRoomModel,
    toModels: toChatRoomModels
}
const ChatRoomAdapterObject = { [getNameForClass(ChatRoom)]: ChatRoomAdapter }


export { ChatRoomAdapterObject, ChatRoomAdapter }
