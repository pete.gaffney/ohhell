import { AdapterSet, AdapterSpec } from '../lib/storage/adapters/adapterTypes'
import { User } from '../user/User'
import { Entity } from '../lib/storage/storageTypes'
import { toSingleKeyFieldEntity, toBaseModel, getNameForClass } from '../lib/storage/adapters/adapterFunctions'

const userSpec: AdapterSpec = {
    targetClass: User,
    pkFieldName: 'userId',
    skFieldName: undefined,
    kindValue: 'User',
    indexes: {}
}

function toUserEntity(model: User){
    const userEntity = toSingleKeyFieldEntity(model, userSpec)
    return [userEntity]
}

function toUserEntities(models: User[]){
    const nested = models.map(model => toUserEntity(model)) as Array<Entity[]>
    return nested.reduce((acc, val) => acc.concat(val), [])
}

function toUserModels(entityArray: Entity[]){
    const partitionMap = entityArray.reduce(
        (acc: Record<string, Entity[]>, next: Entity) => {
            acc[next.pk]
                ? acc[next.pk].push(next)
                : acc[next.pk] = [next]
            return acc
        }, {}
    )
    const users = Object.values(partitionMap).map(partition => toUserModel(partition))
    return users as User[]
}

function toUserModel(entityArray: Entity[]): User {
    const kindMap = entityArray.reduce(
        (acc: Record<string, Entity[]>, next: Entity) => {
            acc[next.Kind]
                ? acc[next.Kind].push(next)
                : acc[next.Kind] = [next]
            return acc
        }, {}
    )
    const user = toBaseModel<User>(kindMap[User.name][0], userSpec)
    return user
}

const UserAdapter: AdapterSet<User> = {
    spec: userSpec,
    toEntity: toUserEntity,
    toEntities: toUserEntities,
    toModel: toUserModel,
    toModels: toUserModels
}

export const UserAdapterObject = { [getNameForClass(User)]: UserAdapter }

export { UserAdapter }