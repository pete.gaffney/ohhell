import { bootServer } from './bootServer'

async function bootstrap() {
  const { app } = await bootServer()
  await app.listen(3000);
}
bootstrap();
