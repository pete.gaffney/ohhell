const appConstants = {
    apiGatewayVersion: '2018-11-29',
    dynamoApiVersion: '2012-08-10'
}

export default appConstants